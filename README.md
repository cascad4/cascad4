# Cascad4

Cascad4 est une variante du jeu du puissance 4.

## Description
L'objectif fondamentale du puissance 4 reste inchangée: aligner 4 pions avant que l'adversaire ne le fasse. Cependant, à la place de se restreindre à une grille, Cascad4 permet également de jouer sans grille : les pions tombent à la manière d'objets physiques. Aligner 4 pions devient alors une différente histoire.

## Aperçu

![exemple de partie](https://gitlab.com/cascad4/cascad4/-/raw/main/img/partie.png)

## Installation
Bien qu'initialement développé pour Linux, Cascad4 supporte également Windows et MacOS.

### 1. Installer Rust
Installer Rust depuis le [site officiel][rust] si nécessaire.

[rust]: https://www.rust-lang.org/tools/install


### 2. Installer SDL2
Cascad4 utilise `sdl2-rs` qui nécessite d'installer les [bibliothèques de développement SDL][sdl].

[sdl]: https://github.com/Rust-SDL2/rust-sdl2#sdl20-development-libraries


### 3. Installer Cascad4

```bash
git clone https://gitlab.com/cascad4/cascad4
cd cascad4
cargo run -r
```

## Utilisation
Lancer Cascad4 depuis le dossier de téléchargement:
```bash
cargo run -r
```

## DOT

* **11 septembre 2023:** Naissance de Cascad4.
* **15 septembre 2023:** Développement des fondations de Cascad4.
* **22 septembre 2023:** Ébauches de détection de collisions entre 2 formes primitives (approche très mathématique).
* **10 octobre 2023:** Début du traitement de l'aspect physique des collisions.
* **17 octobre 2023:** Première version du moteur de rendu.
* **Vacances de la Toussaint:** Déboggage de la première itération  de Cascad4 et révision de la détection des collisions: approche dichotomique.
* **7 novembre 2023:** Réécriture de Cascad4 : passage de l'approche _a priori_ à 60 pps (pas par seconde) à l'approche _a posteriori_ à 10000 pps. Simplification du modèle pour gagner en performances.
* **14 novembre 2023:** Amélioration du moteur de rendu. Détermination des conditions d'alignement des pions et implémentation.
* **21 novembre 2023:** Peaufinage des conditions d'alignement. 
* **28 novembre 2023:** Amélioration de l'interface; implémentation de parallélisation.
* **5 décembre 2023:** Établissement des conditions de victoire et préparation à la collecte de statistiques.
* **6 février 2023:** Enregistrement des parties
* **12 mars 2023:** Création du robot

## Auteur
Entièrement développé par Lothaire Sicot dans le cadre du TIPE de 2023/2024.
