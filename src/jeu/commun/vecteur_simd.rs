use std::{ops::{Add, Neg, Sub, Mul, Div, AddAssign}, f32::consts::PI};
#[cfg(target_arch = "x86")]
use std::arch::x86::*;
#[cfg(target_arch = "x86_64")]
use std::arch::x86_64::*;

pub type Point = Vecteur; 

pub trait IntoF32 {
    fn into_f32(&self) -> f32;
}

impl IntoF32 for f32 {
    fn into_f32(&self) -> f32 {
        *self
    }
}

impl IntoF32 for f64 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for u32 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for i32 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for usize {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

pub fn signe(x: f32) -> f32 {
    if x > 0. {
        1.
    } else if x < 0. {
        -1.
    } else {
        0.
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Vecteur {
    v: __m128,
}

impl Vecteur {
    pub fn cartesien<U, V>(x: U, y: V) -> Vecteur where U: IntoF32, V: IntoF32 {
        unsafe {
            Vecteur {
                v: _mm_set_ps(
                    x.into_f32(),
                    y.into_f32(),
                    0.,
                    0.,
                ),
            }
        }
    }

    pub fn trigo<U, V>(r: U, theta: V) -> Vecteur where U: IntoF32, V: IntoF32 {
        Vecteur::cartesien(
            r.into_f32() * theta.into_f32().cos(),
            r.into_f32() * theta.into_f32().sin(),
        )
    }
    
    pub fn orthogonal(&self) -> Vecteur {
        Vecteur::cartesien( 
            - self.y(),
            self.x(),
        )
    }

    pub fn unitaire(&self) -> Vecteur {
        *self / self.norme()
    }

    pub fn orthonorme(&self) -> Vecteur {
        self.orthogonal().unitaire()
    }

    pub fn norme(&self) -> f32 {
        (ps(*self, *self)).sqrt()
    }

    pub fn norme_inf(&self) -> f32 {
        f32::max(self.x().abs(), self.y().abs())
    }

    pub fn norme_x(&self, x: f32) -> f32 {
        (self.x().abs().powf(x) + self.y().abs().powf(x)).powf(1. / x)
    }

    pub fn argument(&self) -> f32 {
        let x = self.x();
        let y = self.y();
        if x > 0. {
            (y / x).atan()
        } else if x < 0. {
            (y / x).atan() + PI
        } else if y > 0.{
            PI / 2.
        } else if y < 0. {
            - PI / 2.
        } else {
            f32::NAN
        }
    }

    pub fn set_x<U: IntoF32>(&mut self, x: U) {
        unsafe {
            self.v = _mm_set_ps(
                x.into_f32(),
                self.y(),
                0.,
                0.,
            );
        }
    }

    pub fn set_y<U: IntoF32>(&mut self, y: U) {
        unsafe {
            self.v = _mm_set_ps(
                self.x(),
                y.into_f32(),
                0.,
                0.,
            );
        }
    }

    pub fn x(&self) -> f32 {
        let mut valeurs: [f32; 4] = [0.0; 4];
        
        unsafe {
            _mm_storeu_ps(valeurs.as_mut_ptr(), self.v);
        }

        valeurs[0]
    }

    pub fn y(&self) -> f32 {
        let mut valeurs: [f32; 4] = [0.0; 4];
        
        unsafe {
            _mm_storeu_ps(valeurs.as_mut_ptr(), self.v);
        }

        valeurs[1]
    }
}

impl Add<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn add(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            v: unsafe { _mm_add_ps(self.v, autre.v) },
        }
    }
}

impl Neg for Vecteur {
    type Output = Vecteur;

    fn neg(self) -> Self::Output { 
        Vecteur {
            v: unsafe { _mm_sub_ps(_mm_setzero_ps(), self.v) },
        }
    }
}

impl Sub<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn sub(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            v: unsafe { _mm_sub_ps(self.v, autre.v) },
        }
    }
}

impl Mul<f32> for Vecteur {
    type Output = Vecteur;

    fn mul(self, autre: f32) -> Self::Output {
        Vecteur {
            v: unsafe {
                _mm_mul_ps(self.v, _mm_set_ps(autre, autre, 0., 0.))
            },
        }
    }
}

impl Mul<Vecteur> for f32 {
    type Output = Vecteur;

    fn mul(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            v: unsafe {
                _mm_mul_ps(_mm_set_ps(self, self, 0., 0.), autre.v)
            },
        }
    }
}

impl Div<f32> for Vecteur {
    type Output = Vecteur;

    fn div(self, autre: f32) -> Self::Output {
        self * (1. / autre)
    }
}

impl AddAssign for Vecteur {
    fn add_assign(&mut self, autre: Self) {
        self.v = (*self + autre).v;
    }
}

pub fn distance(v1: Vecteur, v2: Vecteur) -> f32 {
    (v2 - v1).norme()
}

pub fn angle(v1: Vecteur, v2: Vecteur) -> f32 {
    v2.argument() - v1.argument()
}

pub fn ps(v1: Vecteur, v2: Vecteur) -> f32 {
    let mut valeurs: [f32; 4] = [0.0; 4];

    unsafe {
        _mm_storeu_ps(
            valeurs.as_mut_ptr(),
            _mm_dp_ps::<0b11001000>(v1.v, v2.v),
        );
    }
    
    valeurs[0]
}
