use super::vecteur::{Point, Vecteur};
use super::joueur::Joueur;
use super::forme::{Forme, Cercle};

#[derive(Debug, Clone)]
pub struct Pion {
    pub id: usize,
    pub joueur: Joueur,

    pub position: Point,
    pub vitesse: Vecteur,

    pub rayon: f32, // 18mm
}

impl Pion {
    pub fn forme(&self) -> Forme {
        Forme::Cercle(Cercle { centre: self.position, rayon: self.rayon })
    }

    pub fn x(&self) -> f32 {
        self.position.x()
    }

    pub fn y(&self) -> f32 {
        self.position.y()
    }
}
