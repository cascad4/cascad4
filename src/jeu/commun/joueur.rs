#[derive(Debug, Clone, Copy)]
pub enum Joueur {
    J1,
    J2,
}

impl Joueur {
    pub fn en_entier(&self) -> i64 {
        match self {
            Joueur::J1 => 1,
            Joueur::J2 => 2
        }
    }

    pub fn autre(&self) -> Joueur {
        match self {
            Joueur::J1 => Joueur::J2,
            Joueur::J2 => Joueur::J1,
        }
    }
}

impl std::cmp::PartialEq for Joueur {
    fn eq(&self, autre: &Self) -> bool {
        match (self, autre) {
            (Joueur::J1, Joueur::J1) => true,
            (Joueur::J2, Joueur::J2) => true,
            _ => false,
        }
    }

    fn ne(&self, autre: &Self) -> bool {
        match (self, autre) {
            (Joueur::J1, Joueur::J2) => true,
            (Joueur::J2, Joueur::J1) => true,
            _ => false,
        }
    }
}

impl std::ops::Not for Joueur {
    type Output = Self;

    fn not(self) -> Self::Output {
        self.autre()
    }
}

impl From<bool> for Joueur {
    fn from(valeur: bool) -> Self {
        if valeur {
            Joueur::J1
        } else {
            Joueur::J2
        }
    }
}

impl Into<bool> for Joueur {
    fn into(self) -> bool {
        match self {
            Joueur::J1 => true,
            Joueur::J2 => false,
        }
    }
}

