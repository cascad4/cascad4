pub mod forme;
pub mod intersection;
pub mod joueur;
pub mod objet;
#[cfg(not(feature = "simd"))]
pub mod vecteur;
#[cfg(feature = "simd")]
pub mod vecteur_simd;
#[cfg(feature = "simd")]
pub use vecteur_simd as vecteur;

pub use joueur::Joueur;
pub use objet::Pion;
pub use vecteur::Vecteur;
