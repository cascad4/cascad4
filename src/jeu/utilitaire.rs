use super::simulation::modelisation::modele::ModelePion;
use super::simulation::simulation::Simulation;
use super::commun::objet::Pion;
use super::commun::joueur::Joueur;
use super::commun::vecteur::{Point, Vecteur};
use super::commun::forme::{Forme, SolHorizontal, DemiDroiteVerticale};


pub fn nouveau_modele() -> ModelePion {
    ModelePion {
        m: 0.050,
        f: 0.1,
        c: 20.,
        k: 8e3,
        a: 0.1,
    }
}

pub fn nouveau_pion_suivant() -> Pion {
    Pion {
        id: 0,
        joueur: Joueur::J1,
        position: Point::cartesien(0.14, 0.26),
        vitesse: Vecteur::cartesien(0, 0),
        rayon: 0.0195,
    }
}

fn ajouter_cadre(sim: &mut Simulation) {
    sim.ajouter_chassis(
        Forme::Sol(
            SolHorizontal { hauteur: 0.001 }
        )
    );
    sim.ajouter_chassis(
        Forme::DemiDroiteVerticale(
            DemiDroiteVerticale { point: Vecteur::cartesien(0, 0.24) }
        )
    );
    sim.ajouter_chassis(
        Forme::DemiDroiteVerticale(
            DemiDroiteVerticale { point: Vecteur::cartesien(0.28, 0.24) }
        )
    );
}

fn ajouter_grille(sim: &mut Simulation) {
    for i in 1..7 {
        sim.ajouter_chassis(
            Forme::DemiDroiteVerticale(
                DemiDroiteVerticale {
                    point: Vecteur::cartesien(0.04 * i as f32, 0.24)
                }
            )
        );
    }
}

pub fn mettre_cadre(sim: &mut Simulation) {
    sim.vider_chassis();
    ajouter_cadre(sim);
}

pub fn mettre_grille(sim: &mut Simulation) {
    sim.vider_chassis();
    ajouter_cadre(sim);
    ajouter_grille(sim);
}

