#[derive(Debug, Clone)]
pub struct ModelePion {
    pub m: f32, // masse
    pub f: f32, // coefficient de frottement solide
    pub c: f32, // coef d'amortissement
    pub k: f32, // raideur
    pub a: f32, // coef de frottement fluide
}

