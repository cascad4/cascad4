use crate::jeu::commun::objet::Pion;
use crate::jeu::commun::vecteur::{Vecteur, ps};
use crate::jeu::commun::intersection::Intersection;
use super::modele::ModelePion;

pub fn poids(modele : &ModelePion) -> Vecteur {
    Vecteur::cartesien(0, - modele.m * 9.81)
}

pub fn trainee(modele: &ModelePion, pion: &Pion) -> Vecteur {
    - modele.a * pion.vitesse.norme() * pion.vitesse
}

pub fn contact(
    modele: &ModelePion,
    pion1: &Pion,
    pion2: &Pion,
    intersection: &Intersection
) -> Vecteur {
    match intersection {
        Intersection::Ponctuelle(p) => {
            let tangente = p.normale.orthogonal();

            let vitesse_relative = pion2.vitesse - pion1.vitesse;
            let derivee_indentation = ps(vitesse_relative, p.normale);

            let n = modele.k * p.indentation + modele.c * derivee_indentation;

            let vitesse_glissement = ps(vitesse_relative, tangente);
            let t = if vitesse_glissement < 0. {
                - modele.f * n
            } else {
                modele.f * n
            };

            n * p.normale + t * tangente
        },
        Intersection::Vide => Vecteur::cartesien(0, 0),
    }
}

pub fn contact_chassis(
    modele: &ModelePion,
    pion1: &Pion,
    intersection: &Intersection
) -> Vecteur {
    match intersection {
        Intersection::Ponctuelle(p) => {
            let tangente = p.normale.orthogonal();

            let vitesse_relative = - pion1.vitesse;
            let derivee_indentation = ps(vitesse_relative, p.normale);

            let n = modele.k * p.indentation + modele.c * derivee_indentation;

            let vitesse_glissement = ps(vitesse_relative, tangente);
            let t = if vitesse_glissement < 0. {
                - modele.f * n
            } else {
                modele.f * n
            };

            n * p.normale + t * tangente
        },
        Intersection::Vide => Vecteur::cartesien(0, 0),
    }
}

