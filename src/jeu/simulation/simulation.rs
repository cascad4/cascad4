use crate::jeu::commun::{intersection::Intersection, objet::Pion};
use crate::jeu::commun::forme::Forme;
use crate::jeu::commun::intersection::intersection;
use crate::jeu::commun::vecteur::Vecteur;
use super::modelisation::{modele::ModelePion, force};

#[derive(Debug, Clone)]
pub struct Simulation {
    pub pions: Vec<Pion>,
    pub modele: ModelePion,
    pub chassis: Vec<Forme>,
    pub pas: f32,
}

impl Simulation {
    pub fn nouveau(pas: f32, modele: ModelePion, pions: Vec<Pion>) -> Simulation {
        Simulation {
            pions,
            modele,
            chassis: vec![],
            pas,
        }
    }

    pub fn ajouter_pion(&mut self, pion: Pion) {
        self.pions.push(pion);
    }
    
    pub fn vider_pions(&mut self) {
        self.pions.clear();
    } 

    pub fn obtenir_pion(&self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions[i].clone());
            }
        }

        None
    }

    pub fn retirer_pion(&mut self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions.remove(i));
            }
        }

        None
    }

    pub fn ajouter_chassis(&mut self, forme: Forme) {
        self.chassis.push(forme);
    }

    pub fn vider_chassis(&mut self) {
        self.chassis.clear();
    } 

    pub fn etapes(&mut self, n: usize) {
        for _ in 0..n {
            self.etape();
        }
    }

    pub fn etape(&mut self) {
        for i in 0..self.pions.len() {
            self.traiter(i);
        }
    }

    pub fn intersections(&self) -> Vec<Intersection> {
        vec![]
    }

    fn traiter(&mut self, obj: usize) {
        let mut somme = Vecteur::cartesien(0, 0);

        somme += force::poids(&self.modele);
        somme += force::trainee(&self.modele, &self.pions[obj]);

        for i in 0..self.pions.len() {
            if i != obj {
                somme += force::contact(
                    &self.modele,
                    &self.pions[obj],
                    &self.pions[i],
                    &intersection(&self.pions[obj].forme(), &self.pions[i].forme())
                );
            }
        }

        for i in 0..self.chassis.len() {
            somme += force::contact_chassis(
                &self.modele,
                &self.pions[obj],
                &intersection(&self.pions[obj].forme(), &self.chassis[i])
            );
        }

        let acceleration = somme / self.modele.m;
        let vitesse = self.pions[obj].vitesse + acceleration * self.pas;
        self.pions[obj].position += vitesse * self.pas;
        self.pions[obj].vitesse = vitesse;
    }
}

