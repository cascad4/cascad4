use crate::jeu::commun::objet::Pion;
use crate::jeu::commun::forme::Forme;
use crate::jeu::commun::intersection::{Intersection, intersection};
use crate::jeu::commun::vecteur::Vecteur;
use super::modelisation::{modele::ModelePion, force};

#[derive(Debug, Clone)]
pub struct Simulation {
    pub pions: Vec<Pion>,
    pub modele: ModelePion,
    pub chassis: Vec<Forme>,
    pub pas: f32,
    tab: Vec<Vec<Intersection>>,
}

impl Simulation {
    pub fn nouveau(pas: f32, modele: ModelePion, pions: Vec<Pion>) -> Simulation {
        Simulation {
            pions,
            modele,
            chassis: vec![],
            pas,
            #[cfg(feature ="tab_optimisation_alloc")]
            tab: vec![vec![Intersection::Vide; 60];50]
        }
    }

    pub fn ajouter_pion(&mut self, pion: Pion) {
        self.pions.push(pion);
    }

    pub fn vider_pions(&mut self) {
        self.pions.clear();
    } 

    pub fn obtenir_pion(&self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions[i].clone());
            }
        }

        None
    }

    pub fn retirer_pion(&mut self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions.remove(i));
            }
        }

        None
    }

    pub fn ajouter_chassis(&mut self, forme: Forme) {
        self.chassis.push(forme);
    }

    pub fn vider_chassis(&mut self) {
        self.chassis.clear();
    } 

    pub fn etapes(&mut self, n: usize) {
        for _ in 0..n {
            self.etape();
        }
    }

    #[cfg(not(feature ="tab_optimisation_alloc"))]
    pub fn intersections(&self) -> Vec<Vec<Intersection>> {
        let mut intersections = vec![
            vec![Intersection::Vide; self.pions.len() + self.chassis.len()];
            self.pions.len()
        ];

        for i in 0..self.pions.len() {
            let f = self.pions[i].forme();
            for j in i + 1..self.pions.len() {
                let inter = intersection(&f, &self.pions[j].forme());
                intersections[j][i] = inter.inverse();
                intersections[i][j] = inter;
            }
            for j in 0..self.chassis.len() {
                intersections[i][self.pions.len() + j] = {
                    intersection(&f, &self.chassis[j])
                };
            }
        }

        intersections
    }

    #[cfg(feature ="tab_optimisation_alloc")]
    pub fn intersections(&mut self) {
        for i in 0..self.pions.len() {
            let f = self.pions[i].forme();
            for j in i + 1..self.pions.len() {
                let inter = intersection(&f, &self.pions[j].forme());
                self.tab[j][i] = inter.inverse();
                self.tab[i][j] = inter;
            }
            for j in 0..self.chassis.len() {
                self.tab[i][self.pions.len() + j] = {
                    intersection(&f, &self.chassis[j])
                };
            }
        }
    }

    #[cfg(not(feature ="tab_optimisation_alloc"))]
    pub fn etape(&mut self) {
        let intersections = self.intersections();

        for i in 0..self.pions.len() {
            self.traiter(i, &intersections[i]);
            #[cfg(feature ="tab_optimisation_alloc")]
            self.traiter(i);
        }
    }

    #[cfg(feature ="tab_optimisation_alloc")]
    pub fn etape(&mut self) {
        self.intersections();

        for i in 0..self.pions.len() {
            self.traiter(i);
        }
    }

    fn traiter(
        &mut self,
        obj: usize,
        #[cfg(not(feature ="tab_optimisation_alloc"))]
        intersections: &Vec<Intersection>
    ) {
        #[cfg(feature ="tab_optimisation_alloc")]
        let intersections = &self.tab[obj];

        let mut somme = Vecteur::cartesien(0, 0);

        somme += force::poids(&self.modele);
        somme += force::trainee(&self.modele, &self.pions[obj]);

        for i in 0..self.pions.len() {
            somme += force::contact(
                &self.modele,
                &self.pions[obj],
                &self.pions[i],
                &intersections[i]
            );
        }

        for i in 0..self.chassis.len() {
            somme += force::contact_chassis(
                &self.modele,
                &self.pions[obj],
                &intersections[self.pions.len() + i]
            );
        }

        let acceleration = somme / self.modele.m;
        let vitesse = self.pions[obj].vitesse + acceleration * self.pas;
        self.pions[obj].position += vitesse * self.pas;
        self.pions[obj].vitesse = vitesse;
    }
}
