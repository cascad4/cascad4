pub mod modelisation;

#[cfg(not(any(feature = "simu_tab", feature = "simu_compart")))]
pub mod simulation;

#[cfg(feature = "simu_tab")]
pub mod simulation_tableau;
#[cfg(feature = "simu_tab")]
pub use simulation_tableau as simulation;


#[cfg(feature = "simu_compart")]
pub mod simulation_compartimentation;
#[cfg(feature = "simu_compart")]
pub use simulation_compartimentation as simulation;

#[cfg(all(feature = "simu_tab", feature = "simu_compart"))]
compile_error!(
    "Les fonctionnalités simu_tab et simu_compart ne sont pas compatibles."
);

