use std::isize;
use std::ops::RangeInclusive;
use crate::jeu::commun::{intersection::Intersection, objet::Pion};
use crate::jeu::commun::forme::Forme;
use crate::jeu::commun::intersection::intersection;
use crate::jeu::commun::vecteur::Vecteur;
use super::modelisation::{modele::ModelePion, force};

#[derive(Debug, Clone)]
struct Compartiment {
    pions: Vec<usize>,
    chassis: Vec<usize>,
}

impl Compartiment {
    fn nouveau() -> Compartiment {
        Compartiment {
            pions: vec![],
            chassis: vec![],
        }
    }
}

#[derive(Debug, Clone)]
struct Compartiments {
    compartiments: Vec<Vec<Compartiment>>,
}

impl Compartiments {
    pub fn nouveau() -> Compartiments {
        let mut compartiments = vec![];
        
        for _ in 0..9 {
            let mut colonne = vec![];
            for _ in 0..9 {
                colonne.push(Compartiment::nouveau());
            }
            compartiments.push(colonne);
        }

        Compartiments {
            compartiments,
        }
    }

    pub fn vider_pions(&mut self) {
        for colonne in &mut self.compartiments {
            for c in colonne {
                c.pions.clear();
            }
        }
    }

    pub fn vider_chassis(&mut self) {
        for colonne in &mut self.compartiments {
            for c in colonne {
                c.chassis.clear();
            }
        }
    }

    fn indice(&self, x: f32) -> usize {
        ((x * 25.) as isize + 1).clamp(0, 8) as usize
    }

    pub fn compartiment_point(&self, position: &Vecteur) -> (usize, usize) {
        (
            self.indice(position.x()),
            self.indice(position.y())
        )
    }

    pub fn ajouter_pion(&mut self, pion: &Pion) {
        let (i, j) = self.compartiment_point(&pion.position);

        self.compartiments[i][j].pions.push(pion.id);
    }

    pub fn trouver_pion(&self, pion: &Pion) -> Option<(usize, usize)> {
        for i in 0..9 {
            for j in 0..9 {
                if self.compartiments[i][j].pions.contains(&pion.id) {
                    return Some((i, j));
                }
            }
        }

        None
    }

    pub fn retirer_pion(&mut self, pion: &Pion) {
        if let Some((i, j)) = self.trouver_pion(pion) {
            self.compartiments[i][j].pions.retain(|x| *x != pion.id);
        }
    }

    pub fn deplacer_pion(&mut self, pion: &Pion) {
        let (i2, j2) = self.compartiment_point(&pion.position);
        
        if !self.compartiments[i2][j2].pions.contains(&pion.id) {
            if let Some((i1, j1)) = self.trouver_pion(pion) {
                if (i1, j1) != (i2, j2) {
                    self.compartiments[i1][j1].pions.retain(|x| *x != pion.id);
                    self.compartiments[i2][j2].pions.push(pion.id);
                }
            }
        }
    }

    pub fn ajouter_pions(&mut self, pions: &Vec<Pion>) {
        for p in pions {
            self.ajouter_pion(p);
        }
    }

    pub fn ajouter_chassis(&mut self, id: usize, forme: &Forme) {
        match forme {
            Forme::Sol(sol) => {
                let i = self.indice(sol.hauteur);

                for colonne in &mut self.compartiments {
                    colonne[i].chassis.push(id)
                }
            },
            Forme::DemiDroiteVerticale(demidroite) => {
                let (i, j) = self.compartiment_point(&demidroite.point);

                for k in 0..=j {
                    self.compartiments[i][k].chassis.push(id);
                }
            },
            _ => {},
        }
    }
}

#[derive(Debug, Clone)]
pub struct Simulation {
    pub pions: Vec<Pion>,
    compartiments: Compartiments,
    pub modele: ModelePion,
    pub chassis: Vec<Forme>,
    pub pas: f32,
}

impl Simulation {
    pub fn nouveau(
        pas: f32,
        modele: ModelePion,
        pions: Vec<Pion>
    ) -> Simulation {
        let mut compartiments = Compartiments::nouveau();

        compartiments.ajouter_pions(&pions);

        Simulation {
            pions,
            compartiments,
            modele,
            chassis: vec![],
            pas,
        }
    }

    pub fn ajouter_pion(&mut self, pion: Pion) {
        self.compartiments.ajouter_pion(&pion);
        self.pions.push(pion);
    }

    pub fn obtenir_pion(&self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions[i].clone());
            }
        }

        None
    }

    pub fn retirer_pion(&mut self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                self.compartiments.retirer_pion(&self.pions[i]);
                return Some(self.pions.remove(i));
            }
        }

        None
    }

    pub fn vider_pions(&mut self) {
        self.compartiments.vider_pions();
        self.pions.clear();
    } 

    pub fn ajouter_chassis(&mut self, forme: Forme) {
        self.compartiments.ajouter_chassis(self.chassis.len(), &forme);
        self.chassis.push(forme);
    }

    pub fn vider_chassis(&mut self) {
        self.compartiments.vider_chassis();
        self.chassis.clear();
    } 

    pub fn etapes(&mut self, n: usize) {
        for _ in 0..n {
            self.etape();
        }
    }

    #[cfg(not(feature = "compart_parcours_zigzag"))]
    pub fn etape(&mut self) {
        for i in 0..9 {
            for j in 0..9 {
                self.traiter_compartiment(i, j);
            }
        }

        for p in &self.pions {
            self.compartiments.deplacer_pion(p);
        }
    }

    #[cfg(feature = "compart_parcours_zigzag")]
    pub fn etape(&mut self) {
        for i in 0..9 {
            if i % 2 == 0 {
                for j in 0..9 {
                    self.traiter_compartiment(i, j);
                }
            } else {
                for j in (0..9).rev() {
                    self.traiter_compartiment(i, j);
                }
            }
        }

        for p in &self.pions {
            self.compartiments.deplacer_pion(p);
        }
    }

    pub fn intersections(&self) -> Vec<Intersection> {
        vec![]
    }

    fn traiter_compartiment(&mut self, i: usize, j: usize) {
        for obj in self.compartiments.compartiments[i][j].pions.clone() {
            self.traiter(
                obj,
                usize::max(i - 1, 0)..=usize::min(i + 1, 8),
                usize::max(j - 1, 0)..=usize::min(j + 1, 8),
            );
        }
    }

    fn traiter(
        &mut self,
        obj: usize,
        irange: RangeInclusive<usize>,
        jrange: RangeInclusive<usize>
    ) {
        let mut somme = Vecteur::cartesien(0, 0);

        somme += force::poids(&self.modele);
        somme += force::trainee(&self.modele, &self.pions[obj]);
        
        #[cfg(feature = "compart_optimisation_chassis")]
        let mut chassis_traite = vec![];

        for i in irange {
            for j in jrange.clone() {
                for &id in &self.compartiments.compartiments[i][j].pions {
                    if id != obj {
                        somme += force::contact(
                            &self.modele,
                            &self.pions[obj],
                            &self.pions[id],
                            &intersection(
                                &self.pions[obj].forme(),
                                &self.pions[id].forme()
                            )
                        );
                    }
                }

                for &id in &self.compartiments.compartiments[i][j].chassis {
                    #[cfg(feature = "compart_optimisation_chassis")]
                    if chassis_traite.contains(&id) {
                        continue;
                    }
                    #[cfg(feature = "compart_optimisation_chassis")]
                    chassis_traite.push(id);

                    somme += force::contact_chassis(
                        &self.modele,
                        &self.pions[obj],
                        &intersection(
                            &self.pions[obj].forme(),
                            &self.chassis[id]
                        )
                    );
                }
            }
        }

        let acceleration = somme / self.modele.m;
        let vitesse = self.pions[obj].vitesse + acceleration * self.pas;
        self.pions[obj].position += vitesse * self.pas;
        self.pions[obj].vitesse = vitesse;
    }
}

