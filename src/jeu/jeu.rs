use std::sync::mpsc::{Receiver, Sender};
use std::thread::JoinHandle;

use sdl2::pixels::Color;
use sdl2::keyboard::{Scancode, Keycode, Mod};
use sdl2::event::Event;
use sdl2::mouse::MouseButton;

use crate::robot;

use super::simulation::simulation::Simulation;
use super::interface::affichage::Ecran;
use super::interface::dessiner::{
    dessiner_alignements,
    dessiner_equilibre,
    dessiner_formes,
    dessiner_pion_suivant,
    dessiner_pions,
    dessiner_trace,
};
use super::commun::{objet::Pion, forme::Forme, joueur::Joueur};
use super::parallele::{CommandeJeu, InformationJeu, fil_simulation};
use super::utilitaire::{nouveau_pion_suivant, nouveau_modele};
use super::alignement::alignements;
use super::trace::Trace;
use super::fin::{Avancement, Resultat};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Etat {
    Tour,
    Attente(usize),
    Pause,
    Pas(usize),
}

#[derive(Clone)]
pub struct Jeu {
    pub tour: usize,
    pub pions: Vec<Pion>,
    avancement: Avancement,
}

impl Jeu {
    pub fn nouveau() -> Jeu {
        Jeu {
            tour: 0,
            pions: vec![],
            avancement: Avancement::nouveau(50),
        }
    }

    pub fn partie_finie(&self) -> bool {
        self.avancement.partie_finie(&self.pions)
    }

    pub fn resultat(&self) -> Resultat {
        self.avancement.resultat(&self.pions)
    }
}

pub struct MoteurJeu {
    etat: Etat,
    _fil: JoinHandle<()>,

    emmeteur: Sender<CommandeJeu>,
    recepteur: Receiver<InformationJeu>,
    
    jeu: Jeu,

    pion_suivant: Pion,
    chassis: Vec<Forme>,
    
    traces: Vec<Trace>,
}

impl MoteurJeu {
    pub fn nouveau_avec_jeu_capture(jeu: Jeu) -> MoteurJeu {
        let (emmeteur, recepteur_distant) = std::sync::mpsc::channel();
        let (emmeteur_distant, recepteur) = std::sync::mpsc::channel();

        MoteurJeu {
            etat: Etat::Tour,
            _fil: fil_simulation(
                Simulation::nouveau(2e-4, nouveau_modele(), jeu.pions.clone()),
                recepteur_distant,
                emmeteur_distant,
            ),

            emmeteur,
            recepteur,

            jeu: jeu.clone(),

            pion_suivant: nouveau_pion_suivant(),

            chassis: vec![],
            traces: vec![],
        }
    }

    pub fn nouveau_avec_jeu(jeu: &Jeu) -> MoteurJeu {
        MoteurJeu::nouveau_avec_jeu_capture(jeu.clone())
    }

    pub fn nouveau() -> MoteurJeu {
        MoteurJeu::nouveau_avec_jeu_capture(
            Jeu::nouveau()
        )
    }

    pub fn nouvelle_partie(&mut self, grille: bool) {
        self.jeu.tour = 0;

        self.pion_suivant.id = self.jeu.tour;
        self.pion_suivant.joueur = Joueur::from(self.jeu.tour % 2 == 0);

        let _ = self.emmeteur.send(CommandeJeu::RetirerPions);
        
        if grille {
            let _ = self.emmeteur.send(CommandeJeu::MettreGrille);
        } else {
            let _ = self.emmeteur.send(CommandeJeu::MettreCadre);
        }
        
        let _ = self.emmeteur.send(CommandeJeu::EnvoyerChassis);
        
        match self.recepteur.recv().unwrap() {
            InformationJeu::Chassis(chassis) => self.chassis = chassis,
            _ => unreachable!(),
        }
        
        self.jeu.avancement.reinitialiser();
        self.traces.clear()
    }

    pub fn deplacer_pion_suivant(&mut self, x: f32) {
        self.pion_suivant.position.set_x(x.clamp(0.02, 0.26));
    }

    pub fn jouer_pion_suivant(&mut self, trace: bool) {
        let _ = self.emmeteur.send(
            CommandeJeu::AjouterPion(self.pion_suivant.clone())
        );
        
        if trace {
            self.traces.clear();
            
            self.traces.push(Trace::nouveau(&self.pion_suivant));
            for p in &self.jeu.pions {
                self.traces.push(Trace::nouveau(p));
            }
        }

        self.jeu.tour += 1;
        
        self.pion_suivant.id = self.jeu.tour;
        self.pion_suivant.joueur = Joueur::from(self.jeu.tour % 2 == 0);
    }

    pub fn annuler_tour(&mut self) {
        if self.jeu.tour > 0 {
            self.jeu.tour -= 1;

            let _ = self.emmeteur.send(CommandeJeu::RetirerPion(self.jeu.tour));

            self.pion_suivant.id = self.jeu.tour;
            self.pion_suivant.joueur = Joueur::from(self.jeu.tour % 2 == 0);
        }

        self.jeu.avancement.reinitialiser();
    }

    pub fn valider_tour(&mut self) {
        if self.jeu.tour > 0 {
            let p = &self.jeu.pions[self.jeu.tour - 1].position;
            if !(0. < p.x() && p.x() < 0.28 && 0. < p.y() && p.y() < 0.24) {
                self.annuler_tour();
            }
        }
    }

    pub fn actualiser(&mut self) {
        if self.etat != Etat::Pause {
            let _ = self.emmeteur.send(CommandeJeu::EnvoyerPions);
            match self.recepteur.recv().unwrap() {
                InformationJeu::Pions(pions) => self.jeu.pions = pions,
                _ => unreachable!(),
            }
        }
        match self.etat {
            Etat::Attente(delai) => {
                if delai > 0 {
                    self.etat = Etat::Attente(delai - 1);
                } else {
                    self.etat = Etat::Tour;
                    self.valider_tour();
                    #[cfg(feature = "robot")]
                    if self.jeu.tour % 2 == 1 {
                        match robot::jouer(&self.jeu.pions, Joueur::J2) {
                            Some(pos) => {
                                let p = self.pion_suivant.position;
                                
                                self.deplacer_pion_suivant(pos);
                                self.jouer_pion_suivant(false);

                                self.etat = Etat::Attente(10);
                                
                                self.pion_suivant.position = p;
                            },
                            None => {},
                        }
                    }
                }
            },
            Etat::Pas(delai) => {
                if delai > 0 {
                    self.etat = Etat::Pas(delai - 1);
                } else {
                    self.etat = Etat::Pause;
                }
            },
            _ => {},
        }

        if self.etat != Etat::Pause {
            let _ = self.emmeteur.send(CommandeJeu::Simuler);
        }

        self.jeu.avancement.actualiser(alignements(&self.jeu.pions));

        if self.etat != Etat::Pause {
            for trace in &mut self.traces {
                for p in &self.jeu.pions {
                    if p.id == trace.id {
                        trace.actualiser(p);
                    }
                }
            }
        }
    }

    pub fn actualiser_graphique(&mut self, ecran: &Ecran) -> bool {
        self.actualiser();
        self.interface_graphique(ecran)
    }

    pub fn interface_graphique(&mut self, ecran: &Ecran) -> bool {
        for trace in &mut self.traces {
            dessiner_trace(ecran.canvas(), trace, ecran.repere());
        }

        dessiner_formes(
            ecran.canvas(),
            &self.chassis,
            ecran.repere(),
            Color::BLACK
        );
        dessiner_pions(
            ecran.canvas(),
            &self.jeu.pions,
            ecran.repere()
        );
        dessiner_alignements(
            ecran.canvas(),
            &self.jeu.pions,
            &self.jeu.avancement.alignements,
            ecran.repere()
        );

        for eq in robot::equilibres(&self.jeu.pions) {
            dessiner_equilibre(
                ecran.canvas(),
                &eq,
                ecran.repere()
            );
        }
        
        if self.etat == Etat::Tour {
            dessiner_pion_suivant(
                ecran.canvas(),
                &self.pion_suivant,
                ecran.repere()
            );
        }

        let mut evenements = ecran.contexte.event_pump().unwrap();
        let keyboard: Vec<_> = {
            evenements
                .keyboard_state()
                .pressed_scancodes()
                .collect()
        };
        for ev in evenements.poll_iter() {
            match ev {
                Event::Quit {..} | Event::KeyDown {
                    keycode: Some(Keycode::Escape | Keycode::Q),
                    ..
                } => {
                    let _ = self.emmeteur.send(CommandeJeu::Arret);
                    return false;
                },
                Event::MouseMotion { x, y, .. } => {
                    if self.etat == Etat::Tour {
                        self.deplacer_pion_suivant(
                            ecran.repere().importer(x, y).x()
                        );
                    }
                },
                Event::MouseButtonDown { mouse_btn, x, y, .. } => {
                    if self.etat == Etat::Tour {
                        self.deplacer_pion_suivant(
                            ecran.repere().importer(x, y).x()
                        );
                        self.jouer_pion_suivant(
                            mouse_btn == MouseButton::Right
                        );
                        self.etat = Etat::Attente(50);
                    }
                },
                Event::KeyDown {
                    keycode: Some(Keycode::Return),
                    keymod,
                    ..
                } => {
                    self.nouvelle_partie(
                        keymod == Mod::LSHIFTMOD || keymod == Mod::RSHIFTMOD
                        || keyboard.contains(&Scancode::LShift)
                        || keyboard.contains(&Scancode::RShift)
                    );
                    self.etat = Etat::Tour;
                },
                Event::KeyDown { keycode: Some(Keycode::Down), keymod, .. } => {
                    if self.etat == Etat::Tour {
                        self.jouer_pion_suivant(
                            keymod == Mod::LCTRLMOD || keymod == Mod::RCTRLMOD
                            || keyboard.contains(&Scancode::LCtrl)
                            || keyboard.contains(&Scancode::RCtrl)
                        );
                        self.etat = Etat::Attente(50);
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::P), .. } => {
                    if self.etat == Etat::Pause {
                        self.etat = Etat::Pas(1);
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::U), .. } => {
                    self.annuler_tour();
                },
                Event::KeyDown { keycode: Some(Keycode::C), .. } => {
                    self.traces.clear();
                },
                Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
                    if self.etat == Etat::Pause {
                        self.etat = Etat::Attente(10);
                    } else {
                        self.etat = Etat::Pause;
                    }
                },
                _ => {},
            }
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Left) {
            self.pion_suivant.position.set_x(
                (self.pion_suivant.position.x() - 0.003).clamp(0.02, 0.26)
            );
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Right) {
            self.pion_suivant.position.set_x(
                (self.pion_suivant.position.x() + 0.003).clamp(0.02, 0.26)
            );
        }

        true
    }

    pub fn partie_finie(&self) -> bool {
        self.jeu.avancement.partie_finie(&self.jeu.pions)
    }

    pub fn resultat(&self) -> super::fin::Resultat {
        self.jeu.avancement.resultat(&self.jeu.pions)
    }

    pub fn jeu(&self) -> &Jeu {
        &self.jeu
    }

    pub fn initialiser(&mut self) {
        let _ = self.emmeteur.send(CommandeJeu::MettreCadre);
        let _ = self.emmeteur.send(CommandeJeu::EnvoyerChassis);
        match self.recepteur.recv().unwrap() {
            InformationJeu::Chassis(chassis) => self.chassis = chassis,
            _ => unreachable!(),
        }
    }
    
    pub fn initialiser_grille(&mut self) {
        let _ = self.emmeteur.send(CommandeJeu::MettreGrille);
        let _ = self.emmeteur.send(CommandeJeu::EnvoyerChassis);
        match self.recepteur.recv().unwrap() {
            InformationJeu::Chassis(chassis) => self.chassis = chassis,
            _ => unreachable!(),
        }
    }
}

