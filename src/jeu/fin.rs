use super::commun::joueur::Joueur;
use super::commun::objet::Pion;
use super::alignement::Alignement;


#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Resultat {
    Victoire(Joueur),
    Egalite,
}

impl Resultat {
    pub fn en_entier(&self) -> i64 {
        match self {
            Resultat::Egalite => 0,
            Resultat::Victoire(j) => j.en_entier(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Avancement {
    pub alignements: Vec<Alignement>,
    durees: Vec<u32>,

    duree_victoire: u32,
}

impl Avancement {
    pub fn nouveau(duree_victoire: u32) -> Avancement {
        Avancement {
            alignements: vec![],
            durees: vec![],
            duree_victoire
        }
    }

    pub fn actualiser(&mut self, alignements: Vec<Alignement>) {
        let n = self.alignements.len();

        for i in 1..=n {
            if alignements.contains(&self.alignements[n - i]) {
                if self.durees[n - i] < self.duree_victoire {
                    self.durees[n - i] += 1;
                }
            } else {
               self.alignements.remove(n - i);
               self.durees.remove(n - i);
            }
        }

        for a in alignements {
            if !self.alignements.contains(&a) {
                self.alignements.push(a);
                self.durees.push(0);
            }
        }
    }

    fn partie_gagnee(&self) -> bool {
        for d in &self.durees {
            if d >= &self.duree_victoire {
                return true;
            }
        }
        
        false
    }

    pub fn partie_finie(&self, pions: &Vec<Pion>) -> bool {
        pions.len() >= 42 || self.partie_gagnee()
    }

    pub fn resultat(&self, pions: &Vec<Pion>) -> Resultat {
        for (i, d) in self.durees.iter().enumerate() {
            if d >= &self.duree_victoire {
                let p = &pions[self.alignements[i][0]];
                return Resultat::Victoire(p.joueur);
            }
        }
        
        Resultat::Egalite
    }

    pub fn reinitialiser(&mut self) {
        self.alignements.clear();
        self.durees.clear();
    }
}
