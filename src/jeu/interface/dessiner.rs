use crate::jeu::commun::joueur::Joueur;
use crate::jeu::commun::objet::Pion;
use crate::jeu::commun::forme::Forme;
use crate::jeu::commun::vecteur::Point;
use crate::jeu::interface::rendu::dessin::multiligne;
use crate::jeu::interface::rendu::dessin::{cercle_rempli, ligne, cercle};
use crate::jeu::trace::Trace;
use super::affichage::Canvas;
use super::rendu::repere::Repere;

use sdl2::pixels::Color;

pub fn dessiner_pions(
    canvas: &Canvas,
    pions: &Vec<Pion>,
    repere: &Repere
) {
    for pion in pions {
        dessiner_pion(canvas, pion, repere);
    }
}

pub fn dessiner_pion(
    canvas: &Canvas,
    pion: &Pion,
    repere: &Repere
) {
    let c1 = match pion.joueur {
        Joueur::J1 => Color::RGB(0xBB, 0x00, 0x00),
        Joueur::J2 => Color::RGB(0x00, 0x00, 0xFF),
    };
    let c2 = match pion.joueur {
        Joueur::J1 => Color::RGB(0xCC, 0x33, 0x11),
        Joueur::J2 => Color::RGB(0x00, 0x77, 0xBB),
    };
    cercle_rempli(canvas, pion.position, pion.rayon, repere, c1);
    cercle_rempli(canvas, pion.position, pion.rayon * 0.8, repere, c2);
}

pub fn dessiner_pion_suivant(
    canvas: &Canvas,
    pion: &Pion,
    repere: &Repere
) {
    let c1 = match pion.joueur {
        Joueur::J1 => Color::RGBA(0xBB, 0x00, 0x00, 0x90),
        Joueur::J2 => Color::RGBA(0x00, 0x00, 0xFF, 0x90),
    };
    cercle_rempli(canvas, pion.position, pion.rayon, repere, c1);
}

pub fn dessiner_equilibre(
    canvas: &Canvas,
    pion: &Pion,
    repere: &Repere
) {
    let c1 = Color::RGBA(0x00, 0x00, 0x00, 0x20);
    cercle_rempli(canvas, pion.position, pion.rayon, repere, c1);
}

pub fn dessiner_formes(
    canvas: &Canvas,
    formes: &Vec<Forme>,
    repere: &Repere,
    couleur: Color
) {
    for forme in formes {
        dessiner_forme(canvas, forme, repere, couleur);
    }
}

pub fn dessiner_forme(
    canvas: &Canvas,
    forme: &Forme,
    repere: &Repere,
    couleur: Color
) {
    match forme {
        Forme::Cercle(cercle) => cercle_rempli(
            canvas,
            cercle.centre,
            cercle.rayon,
            repere,
            couleur
        ),
        Forme::Sol(sol) => ligne(
            canvas,
            Point::cartesien(0, sol.hauteur),
            Point::cartesien(repere.largeur_cadre, sol.hauteur),
            repere,
            couleur
        ),
        Forme::DemiDroiteVerticale(droite) => ligne(
            canvas,
            droite.point,
            Point::cartesien(droite.point.x(), 0),
            repere,
            couleur
        ),
    }
}

pub fn dessiner_alignements(
    canvas: &Canvas,
    pions: &Vec<Pion>,
    alignements: &Vec<Vec<usize>>,
    repere: &Repere
) {
    for a in alignements {
        dessiner_alignement(
            canvas,
            vec![&pions[a[0]], &pions[a[1]], &pions[a[2]], &pions[a[3]]],
            repere
        );
    }
}

pub fn dessiner_alignement(
    canvas: &Canvas,
    pions: Vec<&Pion>,
    repere: &Repere
) {
    let n = pions.len();
    assert!(n == 4);

    let c = match pions[0].joueur {
        Joueur::J1 => Color::RGB(100, 0, 0),
        Joueur::J2 => Color::RGB(0, 0, 100),
    };
    
    multiligne(
        canvas,
        pions[0].position,
        pions[1].position,
        pions[2].position,
        pions[3].position,
        repere,
        c,
        0.005
    );
}

pub fn dessiner_trace(
    canvas: &Canvas,
    trace: &Trace,
    repere: &Repere
) {
    let c = match trace.joueur {
        Joueur::J1 => Color::RGBA(0xBB, 0x00, 0x00, 0x90),
        Joueur::J2 => Color::RGBA(0x00, 0x00, 0xFF, 0x90),
    };
    for i in 0..(trace.positions.len() - 1) {
        let deplacement = (
            trace.positions[i] - trace.positions[i + 1]
        ).norme();
        if deplacement > 0.001 {
            ligne(
                canvas,
                trace.positions[i],
                trace.positions[i + 1],
                repere,
                Color::RGB(200, 200, 200)
            );
        }
    }
    for i in 0..(trace.positions.len() / 2) {
        let deplacement = (
            trace.positions[2 * i] - trace.positions[2 * i + 1]
        ).norme();
        if deplacement > 0.001 {
            cercle(canvas, trace.positions[2 * i], trace.rayon, repere, c);
        }
    }
}

