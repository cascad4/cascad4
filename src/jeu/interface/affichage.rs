use super::rendu::repere::Repere;

use sdl2::{self, pixels::Color, EventPump};


pub type Canvas = sdl2::render::Canvas<sdl2::video::Window>;

pub struct Ecran {
    pub contexte: sdl2::Sdl,
    pub video: sdl2::VideoSubsystem,
    pub canvas: Canvas,
    pub hauteur: u32,
    pub largeur: u32,
    pub repere: Repere,
}

impl Ecran {
    pub fn nouveau(titre: &str, largeur: u32, hauteur: u32) -> Ecran {
        let contexte = sdl2::init().unwrap();
        let video = contexte.video().unwrap();
        let fenetre = video.window(titre, largeur, hauteur)
            .position_centered()
            .build()
            .unwrap();
        let canvas = fenetre.into_canvas().build().unwrap();
        Ecran {
            canvas,
            video,
            contexte,
            hauteur,
            largeur,

            repere: Repere::pour_ecran(hauteur, largeur, 0.28, 0.28),
        }
    }

    pub fn nettoyer(&mut self) {
        self.canvas.set_draw_color(Color::WHITE);
        self.canvas.clear();
    } 

    pub fn actualiser(&mut self) {
        self.canvas.present(); 
    }

    pub fn canvas(&self) -> &Canvas {
        &self.canvas
    }

    pub fn repere(&self) -> &Repere {
        &self.repere
    }

    pub fn evenements(&self) -> EventPump {
        self.contexte.event_pump().unwrap()
    }
}

