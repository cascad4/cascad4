use crate::jeu::commun::vecteur::{Vecteur, Point, IntoF32};

#[derive(Debug, Clone)]
pub struct Repere {
    pub echelle: f32,
    origine_x: i16,
    origine_y: i16,
    pub hauteur_cadre: f32,
    pub largeur_cadre: f32,
}

impl Repere {
    pub fn pour_ecran(
        hauteur_ecran: u32,
        largeur_ecran: u32,
        hauteur_cadre: f32,
        largeur_cadre: f32
    ) -> Repere {
        let echelle_x = largeur_ecran as f32 / largeur_cadre;
        let echelle_y = hauteur_ecran as f32 / hauteur_cadre;
        let echelle = f32::min(echelle_x, echelle_y);
        
        let origine_x = {
            largeur_ecran as i16 / 2 
            - (echelle * largeur_cadre) as i16 / 2
        };
        let origine_y = hauteur_ecran as i16;

        Repere {
            echelle,
            origine_x,
            origine_y,
            hauteur_cadre,
            largeur_cadre,
        }
    }

    pub fn projeter(&self, v: Vecteur) -> (i16, i16) {
        let x = self.origine_x + (v * self.echelle).x() as i16;
        let y = self.origine_y - (v * self.echelle).y() as i16;

        (x, y)
    }

    pub fn importer<U>(&self, x: U, y:U) -> Point where U: IntoF32 {
        let x = (x.into_f32() - self.origine_x as f32) / self.echelle;
        let y = (self.origine_y as f32 - y.into_f32()) / self.echelle;

        Point::cartesien(x, y)
    }

    pub fn convertir(&self, x: f32) -> i16 {
        (x * self.echelle) as i16
    }
}

