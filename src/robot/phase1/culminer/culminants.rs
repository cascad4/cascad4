use crate::jeu::commun::objet::Pion;

use super::segment::Segment;


#[derive(Debug, Clone)]
pub struct ParcoursHautBas {
    ordonnee: f32,
    pions: Vec<Pion>,
}

impl ParcoursHautBas {
    pub fn nouveau(pions: &Vec<Pion>) -> ParcoursHautBas {
        ParcoursHautBas {
            ordonnee: f32::INFINITY,
            pions: pions.clone(),
        }
    }
}

impl Iterator for ParcoursHautBas {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let mut suivant = None;
        let mut ordonnee_max = 0.;

        for pion in &self.pions {
            if pion.y() < self.ordonnee
                && pion.y() > ordonnee_max {
                suivant = Some(pion.id);
                ordonnee_max = pion.y();
            }
        }

        self.ordonnee = ordonnee_max;

        suivant
    }
}

pub fn trouver_culminants(pions: &Vec<Pion>) -> Vec<usize> {
    let mut culminants = vec![];
    let mut segments = vec![];

    for i in ParcoursHautBas::nouveau(pions) {
        let seg = Segment::depuis_pion(&pions[i]);

        if !seg.intersecte(&segments) {
            culminants.push(i);
        }

        segments.push(seg)
    }

    culminants
}

