use crate::jeu::commun::{Pion, Joueur};
use self::culminants::trouver_culminants;

pub mod culminants;
pub mod segment;

pub fn culminer(pions: &Vec<Pion>, joueur: Joueur) -> Option<f32> {
    let culminants = trouver_culminants(pions);

    for i in culminants {
        if pions[i].joueur != joueur {
            if (0.030..0.204).contains(&pions[i].y()) {
                return Some(pions[i].x());
            }
        }
    }

    None
}
