use std::ops::Range;

use crate::jeu::commun::objet::Pion;

#[derive(Debug, Clone)]
pub struct Segment {
    intervalle: Range<f32>,
}

impl Segment {
    pub fn depuis_pion(pion: &Pion) -> Segment {
        let debut = pion.position.x() - pion.rayon * 0.9;
        let fin = pion.position.x() + pion.rayon * 0.9;

        Segment {
            intervalle: debut..fin,
        }
    }

    pub fn intersecte(&self, autres: &Vec<Segment>) -> bool {
        for seg in autres {
            if self.intervalle.contains(&seg.intervalle.start)
                || self.intervalle.contains(&seg.intervalle.end) {
                return true;
            }
        }

        false
    }
}
