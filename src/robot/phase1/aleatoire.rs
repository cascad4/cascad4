use rand::Rng;


pub fn jouer() -> Option<f32> {
    Some(
        rand::thread_rng().gen_range(0.02..0.26)
    )
}

