use crate::jeu::commun::{objet::Pion, joueur::Joueur};

use self::{culminer::culminer, separer::separer};

mod aleatoire;
mod culminer;
mod separer;

pub fn jouer(pions: &Vec<Pion>, joueur: Joueur) -> Option<f32> {
    match culminer(pions, joueur) {
        Some(pos) => return Some(pos),
        None => {},
    }

    match separer(pions, joueur) {
        Some(pos) => return Some(pos),
        None => {},
    }

    aleatoire::jouer()
}

pub fn en_cours(pions: &Vec<Pion>) -> bool {
    let mut nb_pions = 0; // nb de pions sur la 1ere ligne

    for p in pions {
        if p.position.y() < 0.030 {
            nb_pions += 1;
        }
    }

    nb_pions < 7
}

