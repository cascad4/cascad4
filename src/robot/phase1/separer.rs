use crate::jeu::commun::{objet::Pion, joueur::Joueur};


fn plus_proches(pions: &Vec<Pion>, joueur: Joueur) -> Option<(usize, usize)> {
    let mut couple = None;
    let mut distance = f32::INFINITY;
    
    for i in 0..pions.len() {
        for j in 0..pions.len() {
            if i != j 
                && pions[i].joueur != joueur
                && pions[j].joueur != joueur
                && pions[i].position.y() < 0.030
                && pions[j].position.y() < 0.030 {
                let dist = (
                    pions[j].position
                    - pions[i].position
                ).norme_x(5.0);
                if dist < distance {
                    couple = Some((i, j));
                    distance = dist;
                }
            }
        }
    }

    couple
}

pub fn separer(pions: &Vec<Pion>, joueur: Joueur) -> Option<f32> {
    if let Some((i, j)) = plus_proches(pions, joueur) {
        Some(
            (pions[i].position.x() + pions[j].position.x()) / 2.
        )
    } else {
        None
    }
}

