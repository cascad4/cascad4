use crate::jeu::commun::{joueur::Joueur, objet::Pion, vecteur::Vecteur};

use self::phase2::equilibres::trouver_equilibres;

mod phase1;
mod phase2;

pub fn jouer(pions: &Vec<Pion>, joueur: Joueur) -> Option<f32> {
    if phase1::en_cours(pions) {
        phase1::jouer(pions, joueur)
    } else {
        phase2::jouer(pions, joueur)
    }
}

pub fn equilibres(pions: &Vec<Pion>) -> Vec<Pion> {
    let mut equilibres = vec![];

    for eq in trouver_equilibres(pions) {
        equilibres.push(Pion {
            id: 0,
            joueur: Joueur::J2,
            position: eq,
            vitesse: Vecteur::cartesien(0, 0),
            rayon: 0.019,
        });
    }

    equilibres
}

