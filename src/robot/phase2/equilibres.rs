use crate::jeu::commun::{objet::Pion, vecteur::Vecteur};

impl Vecteur {
    pub fn dessus(pion: &Pion) -> Vecteur {
        pion.position + Vecteur::cartesien(0, 2. * pion.rayon)
    }

    pub fn proximite(pion: usize, pions: &Vec<Pion>) -> Vec<Vecteur> {
        let mut equilibres = vec![];

        for i in voisins(pion, pions) {
            match Vecteur::intercale(&pions[pion], &pions[i]) {
                Some(eq) => equilibres.push(eq),
                None => {},
            }
        }

        match Vecteur::intercale_paroi(&pions[pion], 0.00) {
            Some(eq) => equilibres.push(eq),
            None => {},
        }


        match Vecteur::intercale_paroi(&pions[pion], 0.28) {
            Some(eq) => equilibres.push(eq),
            None => {},
        }
        equilibres
    }

    pub fn intercale(pion1: &Pion, pion2: &Pion) -> Option<Vecteur> {
        let r = pion1.rayon;
        
        let p1 = pion1.position;
        let p2 = pion2.position;

        let d = (p2 - p1).norme();

        if d >= 4. * r {
            return None;
        }

        let milieu = (p1 + p2) / 2.;

        let normale = (p2 - p1).unitaire();
        let tangente = normale.orthogonal();
        
        let l = (4. * r.powi(2) - d.powi(2) / 4.).sqrt();

        if p1.x() + 1.8 * r < p2.x() {
            let p3 = milieu + l * tangente;
            if !(0.001 + p1.x()..p2.x() + 0.001).contains(&p3.x()) {
                return None;
            }

            return Some(p3);
        }

        if p2.x() < p1.x() - 1.8 * r {
            let p3 = milieu - l * tangente;
            if !(0.001 + p2.x()..p1.x() + 0.001).contains(&p3.x()) {
                return None;
            }

            return Some(p3);
        }

        None
    }

    pub fn intercale_paroi(pion: &Pion, paroi: f32) -> Option<Vecteur> {
        let r = pion.rayon;
        let p = pion.position;
        
        let d = (p.x() - paroi).abs();

        if d > 2.8 * r {
            return None;
        }

        let l = (4. * r.powi(2) - (d - r).powi(2)).sqrt();

        if p.x() < paroi {
            Some(Vecteur::cartesien(paroi - r, p.y() + l))
        } else {
            Some(Vecteur::cartesien(paroi + r, p.y() + l))
        }
    }
}

pub fn trouver_equilibres(pions: &Vec<Pion>) -> Vec<Vecteur> {
    let mut equilibres = vec![];
    // let culminants = trouver_culminants(pions);

    // Les positions d'équilibre sont à proximité des culminants
    for i in 0..pions.len() {
        equilibres.append(&mut Vecteur::proximite(i, pions));
    }

    let mut i = 0;
    'boucle_suppression: while i < equilibres.len() {
        // Suppression des équilibres inaccessibles
        if equilibres[i].y() > 0.24 {
            equilibres.remove(i);
            continue 'boucle_suppression;
        }

        // Suppression des équilibres impossibles
        for pion in pions {
            let distance = (
                pion.position
                - equilibres[i]
            ).norme();
            if distance < 1.9 * pion.rayon {
                equilibres.remove(i);
                continue 'boucle_suppression;
            }
        }

        // Suppression des doublons
        for j in 0..i {
            if (equilibres[i].x() - equilibres[j].x()).abs() < 0.003 {
                if equilibres[i].y() > equilibres[j].y() {
                    equilibres[j] = equilibres.remove(i);
                } else {
                    equilibres.remove(i);
                }
                continue 'boucle_suppression;
            }
        }
       
        i += 1;
    }

    equilibres
}

fn voisins(pion: usize, pions: &Vec<Pion>) -> Vec<usize> {
    let mut voisins = vec![];
    let r = pions[pion].rayon;

    for i in 0..pions.len() {
        if i != pion {
            let d = (pions[pion].position - pions[i].position).norme();
            
            if d < 3.8 * r {
                voisins.push(i);
            }
        }
    }

    voisins
}

