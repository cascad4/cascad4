use crate::jeu::commun::{joueur::Joueur, objet::Pion};

use self::minimax::minimax;

pub mod equilibres;
pub mod minimax;

pub fn jouer(pions: &Vec<Pion>, joueur: Joueur) -> Option<f32> {
    let mut vmax = f32::NEG_INFINITY;
    let mut pos_max = None;

    for (v, pos) in minimax(pions, joueur) {
        if v > vmax {
            vmax = v;
            pos_max = Some(pos.x());
        }
    }

    println!("{}",vmax);

    pos_max
}

