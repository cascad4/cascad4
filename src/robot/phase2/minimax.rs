use crate::jeu::{
    alignement::alignements_quelconques,
    commun::{
        joueur::Joueur,
        objet::Pion,
        vecteur::Vecteur
    }
};

use super::equilibres::trouver_equilibres;

const PROFONDEUR_MAX: usize = 6;

#[derive(Debug)]
struct Noeud {
    pions: Vec<Pion>,
    enfants: Vec<Noeud>,
}

pub fn minimax(pions: &Vec<Pion>, joueur: Joueur) -> Vec<(f32, Vecteur)> {
    let tour = pions.len();
    let equilibres = trouver_equilibres(pions);

    let mut valeurs = vec![];

    let mut fils = vec![];

    for position in equilibres {
        let pions = pions.clone();
        fils.push(std::thread::spawn(move || {(
            alphabeta(
                Noeud::nouveau(&pions, position),
                tour,
                f32::NEG_INFINITY,
                f32::INFINITY,
                joueur,
                PROFONDEUR_MAX,
            ),
            position
        )}));
    }
    for fil in fils {
        let (valeur, position) = {
            fil.join().expect("Impossible de récupérer le fil.")
        };

        valeurs.push((valeur, position))
    }

    valeurs
}

fn alphabeta(
    mut noeud: Noeud,
    tour: usize,
    mut alpha: f32,
    beta: f32,
    joueur: Joueur,
    profondeur: usize,
) -> f32 {
    let alignements = noeud.construire();

    if noeud.est_feuille(&alignements) || profondeur == 0 {
        noeud.valeur(&alignements, joueur)
    } else {
        let mut valeur = f32::NEG_INFINITY;

        for fils in noeud.enfants() {
            valeur = f32::max(
                valeur,
                - alphabeta(
                    fils,
                    tour,
                    - beta,
                    - alpha,
                    joueur,
                    profondeur - 1,
                )
            );
            
            if valeur >= beta {
                return valeur; // coupure beta
            }
            
            alpha = f32::max(alpha, valeur);
        }

        valeur
    }
}

impl Noeud {
    fn nouveau(pions: &Vec<Pion>, position: Vecteur) -> Noeud {
        let pion = Pion {
            id: pions.len(),
            joueur: Joueur::from(pions.len() % 2 == 0),
            position,
            vitesse: Vecteur::cartesien(0, 0),
            rayon: 0.0195,
        };

        Noeud {
            pions: [pions.clone(), vec![pion]].concat(),
            enfants: vec![],
        }
    }

    fn construire(&mut self) -> Vec<Vec<usize>> {
        let equilibres = trouver_equilibres(&self.pions);

        for position in equilibres {
            self.enfants.push(Noeud::nouveau(
                &self.pions,
                position,
            ));
        }

        alignements_quelconques(&self.pions)
    }

    fn est_feuille(&self, alignements: &Vec<Vec<usize>>) -> bool {
        self.enfants.is_empty()
        || self.partie_finie(alignements)
    }

    fn enfants(self) -> Vec<Noeud> {
        self.enfants
    }

    fn partie_finie(&self, alignements: &Vec<Vec<usize>>) -> bool {
        for alignement in alignements {
            if alignement.len() >= 4 {
                return true;
            }
        }

        false
    }

    fn valeur(&mut self, alignements: &Vec<Vec<usize>>, joueur: Joueur) -> f32 {
        let mut valeur = 0.;

        for align in alignements {
            let joueur_align = self.pions[align[0]].joueur;

            valeur += if joueur_align == joueur {
                echelle_valeur(align)
            } else {
                - echelle_valeur(align)
            };
        }

        valeur
    }
}

fn echelle_valeur(alignement: &Vec<usize>) -> f32 {
    match alignement.len() {
        4 => 1000f32,
        _ => 0f32,
    }
}

