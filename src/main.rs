pub mod aide;
pub mod commandes;
pub mod jeu;
pub mod perf;
pub mod robot;
pub mod stats;

use std::env::args;

use sdl2::gfx::framerate::FPSManager;

use commandes::ArgumentStat;
use jeu::interface::affichage::Ecran;
use jeu::jeu::MoteurJeu;
use stats::enregistrer::Enregistreur;


fn interface_graphique() {
    let mut ecran = Ecran::nouveau("Cascad4", 720, 480);
    let mut jeu = MoteurJeu::nouveau();

    jeu.initialiser();

    let mut fps = FPSManager::new();
    let _ = fps.set_framerate(50);

    let mut continuer = true;
    while continuer {
        ecran.nettoyer();
        continuer = jeu.actualiser_graphique(&ecran);
        ecran.actualiser();
        fps.delay();
    }
}


fn main() {
    let mut args = args();
    
    args.next();
    match args.next().as_deref() {
        Some("jeu") => interface_graphique(),
        Some("aide") => {
            match args.next().map(|x| {x.to_lowercase()}).as_deref() {
                Some("aide") => println!("{}", aide::AIDE_AIDE),
                Some("jeu") => println!("{}", aide::AIDE_JEU),
                Some("stat") => println!("{}", aide::AIDE_STAT),
                Some("espace") => println!("{}", aide::AIDE_ESPACE),
                Some("temps") => println!("{}", aide::AIDE_TEMPS),
                Some("repartition") => println!("{}", aide::AIDE_REPARTITION),
                Some(rubrique) => println!(
                    "Rubrique inconnue: {}\n{}",
                    rubrique,
                    aide::AIDE_AIDE
                ),
                None => println!("{}", aide::AIDE),
            }
        },
        Some("stat") => {
            let mut enr = Enregistreur::nouveau();

            'arguments: loop {
                match ArgumentStat::extraire(&mut args) {
                    Ok(Some(ArgumentStat::NbFils(nb_fils))) => {
                        enr.nb_fils = nb_fils.en_entier();
                    },
                    Ok(Some(ArgumentStat::NbParties(nb_parties))) => {
                        enr.nb_parties = nb_parties.en_entier();
                    },
                    Ok(Some(ArgumentStat::CheminDB(chemin))) => {
                        enr.chemin_db = chemin;
                    },
                    Ok(Some(ArgumentStat::Grille(grille))) => {
                        enr.conf.grille = grille.en_liste();
                    },
                    Ok(Some(ArgumentStat::EcartsEspace(espace))) => {
                        enr.perturbations.ecarts_espace = espace.en_liste();
                    },
                    Ok(Some(ArgumentStat::EcartsTemps(temps))) => {
                        enr.perturbations.ecarts_temps = temps.en_liste();
                    },
                    Ok(Some(ArgumentStat::Repartitions1(rep))) => {
                        enr.conf.repartitions1 = rep.en_liste();
                    },
                    Ok(Some(ArgumentStat::Repartitions2(rep))) => {
                        enr.conf.repartitions2 = rep.en_liste();
                    },
                    Ok(None) => break 'arguments,
                    Err(msg) => {
                        eprintln!("{}", msg);
                        return;
                    },
                }
            }

            enr.initialiser_db();
            enr.enregistrer();
        },
        Some("perf") => {
            perf::mesure::afficher_perf();
        },
        Some("perf-simu") => {
            perf::mesure::afficher_perf_simu();
        },
        Some(_) => println!("{}", aide::AIDE),
        _ => interface_graphique(),
    }
}
