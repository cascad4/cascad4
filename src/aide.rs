pub const AIDE: &str = r#"
=%%= Utilisation de cascad4 =%%=
Depuis l'exécutable:
$ cascad4 [commande [...]]
Avec cargo:
$ cargo run -r -- [commande [...]]

Commandes: jeu, stat, aide
    jeu (par défaut): lance l'interface graphique de cascad4 ($ cascad4 aide jeu)
    stat: enregistre des parties de cascad4 ($ cascad4 aide stat)
    aide: affiche ce message ($ cascad4 aide aide)
"#;

pub const AIDE_AIDE: &str = r#"
Utilisation de la commande aide:
$ cascad4 aide [<rubrique>]

Affiche l'aide de la rubrique.

Rubriques pricipales: aide, jeu, stat
Rubriques secondaires: espace, temps, repartition
"#;

pub const AIDE_JEU: &str = r#"
Utilisation de l'interface graphique:
$ cascad4 jeu

Contrôles de l'interface graphique:
    Déplacer le curseur: souris ou Flèches gauche/droite
    Placer le pion: Clic gauche ou Flèche bas
    Placer le pion avec la trace: Clic droit ou Crtl gauche + Flèche bas
    Nettoyer la trace: Touche C
    Retirer le dernier pion: Touche U
    Mettre en pause/reprendre: Touche espace
    Nouvelle partie (sans colonnes): Touche entrée
    Nouvelle partie avec colonnes: Maj gauche + Touche entrée
    Quitter cascad4: Touche échappe
"#;

pub const AIDE_STAT: &str = r#"
Enregistrement des parties:
$ cascad4 stat [...] [<nbparties>]

nbparties: nombre de parties à enregistrer (entier naturel, 10 par défaut)

Arguments:
    --cadre, -c: Enregistre les parties sans la grille (par défaut)
    --grille, -g: Enregistre les parties avec la grille (puissance 4 classique)
    --espace=<valeurs>, -e <valeurs>: Écarts type des perturbations spaciales
        ($ cascad4 aide espace)
    --temps=<valeurs>, -t <valeurs>: Écarts types des perturbations temporelles
        ($ cascad4 aide temps)
    --rep1=<repartitions>, -1 <repartitions>: Répartitions des coups du 1er joueur
    --rep2=<repartitions>, -2 <repartitions>: Répartitions des coups du 2nd joueur
        ($ cascad4 aide repartition)
    --nb-fils=<nbfils>, -f <nbfils>: Nombre de parties en simultané (entier > 0)
    --chemin-db=<chemin>, -c <chemin>: Chemin de la base de donnée
    --nb-partie=<nbparties>, -n <nbparties>: nombre de parties à enregistrer
"#;

pub const AIDE_ESPACE: &str = r#"
Perturbations spaciales:
$ cascad4 stat --espace=<valeurs> [...]
$ cascad4 stat -e <valeurs> [...]

<valeurs>: liste des écarts type (flottants positifs) de la forme
    <valeur>,<valeur>,...,<valeur>
<valeur>: ecart type en px (flottant >= 0)

Note:
Le diamètre d'un pion est 67 px.
La largeur du plateau est 480 px.

Exemples:
$ cascad4 stats --espace=0
La commande enregistre les mêmes parties avec des perturbations
spaciales de 0 px

$ cascad4 stats --espace=0,0.5,1,1.5
La commande enregistre les mêmes parties avec des perturbations
spaciales de 0, 0.5, 1 et 1.5 px

$ cascad4 stats -e 2,4
La commande enregistre les mêmes parties avec des perturbations
spaciales de 2 et 4 px
"#;

pub const AIDE_TEMPS: &str = r#"
Perturbations temporelles:
$ cascad4 stat --temps=<valeurs> [...]
$ cascad4 stat -t <valeurs> [...]

<valeurs>: liste des écarts type (flottants positifs) de la forme
    <valeur>,<valeur>,...,<valeur>
<valeur>: ecart type en image (flottant >= 0)

Note:
Le jeu tourne à 50Hz.
Une image dure donc 20ms.

Exemples:
$ cascad4 stats --temps=0
La commande enregistre les mêmes parties avec des perturbations
temporelles de 0 images

$ cascad4 stats --temps=0,2,4
La commande enregistre les mêmes parties avec des perturbations
temporelles de 0, 2, et 4 images

$ cascad4 stats -t 0,0.5,1
La commande enregistre les mêmes parties avec des perturbations
temporelles de 0, 0.5, et 1 images
"#;

pub const AIDE_REPARTITION: &str = r#"
Répartitions:
$ cascad4 stat --repX=<repartitions> [...]
$ cascad4 stat -X <repartitions> [...]

X: 1 ou 2
<repartitons>: liste des répartitions, de la forme
    <repartition>,<repartition>,...,<repartition>
<reparition>: une des options ci-dessous

Répartitions:
    uniforme: les pions sont placés selon une loi uniforme continue
    segment[<a>,<b>]: les pions sont placés selon une loi uniforme
        sur un segment [a;b] avec:
            <a>: flottant entre 0 et 1
            <b>: flottant entre 0 et 1 supérieur à <a>
    beta[<alpha>,<beta>]: les pions sont placés selon une loi bêta de
        paramètres <alpha> et <beta> avec:
            <alpha>: flottant positif
            <beta>: flottant positif

Note:
Si l'argument --repX est omis, la repartition du joueur X sera
la repartition uniforme.
"#;

