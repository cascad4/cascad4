use crate::jeu::{
    jeu::{Jeu, MoteurJeu},
    simulation::simulation::Simulation,
    utilitaire::nouveau_modele
};
use rand::Rng;

pub trait Generer {
    fn generer(pions: usize, imgs: usize) -> Self;
}

impl Generer for Jeu {
    fn generer(pions: usize, imgs: usize) -> Self {
        let mut moteur = MoteurJeu::nouveau();
        #[cfg(not(feature="perf_grille"))]
        moteur.initialiser();
        #[cfg(feature="perf_grille")]
        moteur.initialiser_grille();
        let mut rng = rand::thread_rng();
        
        for _ in 0..pions {
            for __ in 0..100 {
                moteur.actualiser();
            }
            moteur.deplacer_pion_suivant(rng.gen_range(0.02..0.26));
            moteur.jouer_pion_suivant(false);
        }
        for _ in 0..imgs {
            moteur.actualiser();
        }

        moteur.jeu().clone()
    }
}

impl Generer for Simulation {
    fn generer(pions: usize, imgs: usize) -> Self {
        Simulation::nouveau(
            2e-4,
            nouveau_modele(),
            Jeu::generer(pions, imgs).pions,
        )
    }
}
