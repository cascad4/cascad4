use std::io::{stderr, Write};
use std::thread::JoinHandle;
use std::time::{Duration, Instant};
use std::sync::{Arc, Mutex};
use rand::Rng;

use crate::jeu::interface::dessiner::dessiner_pions;
use crate::jeu::{
    alignement::alignements as align,
    interface::affichage::Ecran,
    jeu::{Jeu, MoteurJeu},
    simulation::simulation::Simulation
};

use super::generer::Generer;

type REcran = Arc<Mutex<Ecran>>;

fn etape(mut simu: Simulation) -> Duration {
    let debut = Instant::now();

    simu.etape();

    debut.elapsed()
}

#[cfg(not(feature = "tab_optimisation_alloc"))]
fn intersections(simu: Simulation) -> Duration {
    let debut = Instant::now();

    simu.intersections();

    debut.elapsed()
}

#[cfg(feature = "tab_optimisation_alloc")]
fn intersections(mut simu: Simulation) -> Duration {
    let debut = Instant::now();

    simu.intersections();

    debut.elapsed()
}

fn alignements(jeu: Jeu) -> Duration {
    let debut = Instant::now();

    align(&jeu.pions);

    debut.elapsed()
}

fn actualisation(jeu: Jeu) -> Duration {
    let mut moteur = MoteurJeu::nouveau_avec_jeu_capture(jeu);
    moteur.initialiser();
    
    let debut = Instant::now();
    
    moteur.actualiser();

    debut.elapsed()
}

fn interface((jeu, ecran): (Jeu, REcran)) -> Duration {
    let mut moteur = MoteurJeu::nouveau_avec_jeu_capture(jeu);
    moteur.initialiser();
    
    let mut ecran = ecran.lock().unwrap();
    ecran.nettoyer();
    
    let debut = Instant::now();
    
    moteur.interface_graphique(&ecran);

    let d = debut.elapsed();
    if rand::thread_rng().gen_bool(0.01) {
        ecran.actualiser();
    }
    d
}

fn dessin_pions((jeu, ecran): (Jeu, REcran)) -> Duration {
    let mut ecran = ecran.lock().unwrap();
    
    ecran.nettoyer();
    
    let debut = Instant::now();

    dessiner_pions(ecran.canvas(), &jeu.pions, ecran.repere());

    let d = debut.elapsed();
    if rand::thread_rng().gen_bool(0.01) {
        ecran.actualiser();
    }
    d
}

fn rafraichissement((jeu, ecran): (Jeu, REcran)) -> Duration {
    let mut moteur = MoteurJeu::nouveau_avec_jeu_capture(jeu);
    moteur.initialiser();
    let mut ecran = ecran.lock().unwrap();
    
    ecran.nettoyer();
    moteur.interface_graphique(&ecran);
    
    let debut = Instant::now();

    ecran.actualiser(); 

    debut.elapsed()
}

fn mesure_obj<F, O>(
    obj: &O,
    fonction: F,
    nb: u32,
) -> Duration where F: Fn(O) -> Duration, O: Clone {
    let mut somme = Duration::from_millis(0);

    for _ in 0..nb {
        std::thread::yield_now();
        somme += fonction(obj.clone());
    }

    somme / nb
}

fn mesures(pions: usize, imgs: usize, ecran: REcran) -> Vec<Duration> {
    let mut mesures = vec![];

    let simu = Simulation::generer(pions, imgs);
    let jeu = Jeu::generer(pions, imgs);

    mesures.push(mesure_obj(&simu, etape, 500));
    mesures.push(mesure_obj(&simu, intersections, 500));
    mesures.push(mesure_obj(&jeu, actualisation, 500));
    mesures.push(mesure_obj(&jeu, alignements, 500));

    let obj = (jeu, ecran);

    mesures.push(mesure_obj(&obj, interface, 500));
    mesures.push(mesure_obj(&obj, dessin_pions, 500));
    mesures.push(mesure_obj(&obj, rafraichissement, 500));

    mesures
}

fn mesurer(
    pions: Vec<usize>,
    imgs: Vec<usize>
) -> Vec<Vec<Vec<Duration>>> {
    let mut l = vec![];
    let ecran = Arc::new(Mutex::new(Ecran::nouveau("Perf", 720, 480)));

    for p in &pions {
        let mut m = vec![];
        for i in &imgs {
            m.push(mesures(*p, *i, ecran.clone()));
        }
        l.push(m);
    }

    l
}

pub fn afficher_perf() {
    print!("Calcul des performances...");
    let pions: Vec<_> = (0..=42).collect();
    let mesures = mesurer(
        pions.clone(),
        vec![500]
    );
    println!(" OK\n");

    let m = &mesures[pions.len() - 1][0];
    let t = (m[2] + m[4] + m[6]).as_secs_f32();
    println!("Étapes: {:0.2}%", 100. * m[0].as_secs_f32() / t * 100.);
    println!("Intersections: {:0.2}%", 100. * m[1].as_secs_f32() / t * 100.);
    println!("Actualisation: {:0.2}%", m[2].as_secs_f32() / t * 100.);
    println!("Alignements: {:0.2}%", m[3].as_secs_f32() / t * 100.);
    println!("Interface: {:0.2}%", m[4].as_secs_f32() / t * 100.);
    println!("Dessin des pions: {:0.2}%", m[5].as_secs_f32() / t * 100.);
    println!("Rafraîchissement: {:0.2}%", m[6].as_secs_f32() / t * 100.);
    println!();
    println!("CSV:");
    for (i, m) in mesures.iter().enumerate() {
        let m = &m[0];
        let t = (m[2] + m[4] + m[6]).as_secs_f32();
        println!(
            "{};{:0.4};{:0.4};{:0.4};{:0.4};{:0.4};",
            pions[i], // nb pions
            m[2].as_secs_f32() * 1000., // act ms
            m[4].as_secs_f32() * 1000., // iface ms
            m[6].as_secs_f32() * 1000., // rafr ms
            100. * m[0].as_secs_f32() * 1000., // etapes ms
            t * 1000., // total ms
        );
    }
}

fn mesures_simu(pions: usize, nb1: u32, nb2: u32) -> Duration {
    let mut somme = Duration::from_secs(0);
    let mut nombre = 0;

    fn fil(pions: usize, nb2: u32) -> JoinHandle<Duration> {
        std::thread::spawn(move || {
            let simu = Simulation::generer(pions, 100);
            mesure_obj(&simu, etape, nb2)
        })
    }

    let mut fils = vec![];
    
    'principale: while nombre < nb1 {
        if fils.len() < 4 && nombre + (fils.len() as u32) < nb1 {
            fils.push(fil(pions, nb2));
        }

        for i in 0..fils.len() {
            if fils[i].is_finished() {
                match fils.remove(i).join() {
                    Ok(v) => {
                        somme += v;
                        nombre += 1;
                    },
                    Err(_) => {
                        eprintln!("1 mesure ignorée");
                    },
                }
                continue 'principale;
            }
        }
    }

    somme / nb1
}

pub fn afficher_perf_simu() {
    eprintln!("Mesures des performances de la simulation");
    
    let mut stderr = stderr().lock();
    let mut mesures = vec![];
    for nb in 0..=42 {
        write!(stderr, "\rProgression: {:0>2}/43", nb + 1)
            .expect("Impossible d'afficher la progression");
        stderr.flush().unwrap();
        mesures.push(mesures_simu(nb, 20, 20));
    }
    drop(stderr);

    eprintln!();
    eprintln!("Durées des étapes (en ms)");
    
    for v in mesures {
        println!(
            "{}",
            format!("{:0.4}", 100. * v.as_secs_f32() * 1000.)
                .replace(".", ",")
        );
    }
}

