use crate::jeu::commun::objet::Pion;
use super::{
    configuration::Configuration,
    partie::{PartiePerturbee, PartieReference}
};


pub type DB = sqlite::Connection;

pub fn creer_tables(db: &DB) {
    db.execute(r#"
        CREATE TABLE IF NOT EXISTS Partie (
            id INTEGER NOT NULL PRIMARY KEY,
            conf INTEGER NOT NULL,
            resultat INTEGER,
            longueur INTEGER
        );
        CREATE TABLE IF NOT EXISTS Perturbation (
            id INTEGER NOT NULL PRIMARY KEY,
            espace REAL,
            temps INTEGER
        );
        CREATE TABLE IF NOT EXISTS PartiePerturbee (
            reference INTEGER NOT NULL,
            conf INTEGER NOT NULL,
            perturbation INTEGER NOT NULL,
            resultat INTEGER,
            longueur INTEGER,
            score REAL,
            UNIQUE(reference, perturbation)
        );
        CREATE TABLE IF NOT EXISTS Configuration (
            id INTEGER NOT NULL PRIMARY KEY,
            repartition1 TEXT,
            repartition2 TEXT,
            grille INTEGER
        );
        CREATE TABLE IF NOT EXISTS Alignement (
            id INTEGER NOT NULL,
            partie INTEGER,
            longueur INTEGER,
            joueur INTEGER,
            UNIQUE(id, partie)
        );
        CREATE TABLE IF NOT EXISTS Pion (
            id INTEGER NOT NULL,
            partie INTEGER,
            joueur INTEGER,
            x REAL,
            y REAL,
            UNIQUE(id, partie)
        );
        CREATE TABLE IF NOT EXISTS AlignementPion (
            alignement INTEGER NOT NULL,
            pion INTEGER NOT NULL,
            UNIQUE(alignement, pion)
        );
    "#).expect("Impossible de créer les tables");
}

pub fn enregistrer_partie(
    partie: &PartieReference,
    enregistrer_pions: bool,
    enregistrer_alignements: bool,
    db: &DB
) {
    let id = partie.id();
    for i in 0..10 {
        if enregistrer_partie_avec_id(
            id + i,
            partie,
            enregistrer_pions,
            enregistrer_alignements,
            db
        ) {
            return;
        }
    }
    eprintln!("\nImpossible d'enregistrer la partie {}", id);
}

fn enregistrer_partie_avec_id(
    id: i64,
    partie: &PartieReference,
    enregistrer_pions: bool,
    enregistrer_alignements: bool,
    db: &DB
) -> bool {
    let mut s = db.prepare(
        "INSERT INTO Partie (id, conf, resultat, longueur) VALUES (?, ?, ?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, id.into()),
        (2, partie.conf_id().into()),
        (3, partie.resultat().en_entier().into()),
        (4, (partie.longueur() as i64).into()),
    ]).expect("Création de la requête impossible");
    match s.next() {
        Ok(_) => {
            for (i, p) in partie.pions().iter().enumerate() {
                if enregistrer_pions {
                    enregistrer_pion(i as i64, id, p, db);
                }
            }
            for (j, a) in partie.alignements().iter().enumerate() {
                if enregistrer_alignements {
                    enregistrer_alignement(j as i64, id, a, db);
                }
                for (i, _) in partie.pions().iter().enumerate() {
                    if enregistrer_pions && enregistrer_alignements {
                        enregistrer_alignementpion(j as i64, i as i64, db);
                    }
                }
            }
            true
        },
        Err(_) => false,
    }
}

fn enregistrer_alignement(id: i64, partie: i64, align: &Vec<usize>, db: &DB) {
    let mut s = db.prepare(
        r"INSERT OR IGNORE INTO Alignement (id, partie, longueur, joueur)
        VALUES (?, ?, ?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, id.into()),
        (2, partie.into()),
        (3, (align.len() as i64).into()),
        (4, (align[0] as i64 % 2 + 1).into()),
    ]).expect("Création de la requête impossible.");
    s.next().expect("Impossible d'enregistrer l'alignement.");
}

fn enregistrer_pion(id: i64, partie: i64, pion: &Pion, db: &DB) {
    let mut s = db.prepare(
        r"INSERT OR IGNORE INTO Pion (id, partie, joueur, x, y)
        VALUES (?, ?, ?, ?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, id.into()),
        (2, partie.into()),
        (3, pion.joueur.en_entier().into()),
        (4, (pion.position.x() as f64).into()),
        (5, (pion.position.y() as f64).into()),
    ]).expect("Création de la requête impossible");
    s.next().expect("Impossible d'enregistrer le pion");
}

fn enregistrer_alignementpion(alignement: i64, pion: i64, db: &DB) {
    let mut s = db.prepare(
        "INSERT OR IGNORE INTO AlignementPion (alignement, pion) VALUES (?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, alignement.into()),
        (2, pion.into()),
    ]).expect("Création de la requête impossible");
    s.next().expect("Impossible d'enregistrer le lien alignement/pion");
}

pub fn enregistrer_perturbation(id: i64, espace: f32, temps: f32, db: &DB) {
    let mut s = db.prepare(
        "INSERT OR IGNORE INTO Perturbation (id, espace, temps) VALUES (?, ?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, id.into()),
        (2, (espace as f64).into()),
        (3, (temps as f64).into()),
    ]).expect("Création de la requête impossible");
    s.next().expect("Impossible d'enregistrer la perturbation");
}

pub fn enregistrer_partieperturbee(partie: &PartiePerturbee, db: &DB) {
    let mut s = db.prepare(r#"
        INSERT OR IGNORE INTO PartiePerturbee
        (reference, resultat, longueur, perturbation, score, conf)
        VALUES (?, ?, ?, ?, ?, ?)
    "#).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, partie.id().into()),
        (2, partie.resultat().en_entier().into()),
        (3, (partie.longueur() as i64).into()),
        (4, partie.perturbation.id.into()),
        (5, partie.score().into()),
        (6, partie.conf_id().into()),
    ]).expect("Création de la requête impossible");
    s.next().expect("Impossible d'enregistrer la partie");
}

pub fn enregistrer_configuration(conf: &Configuration, db: &DB) {
    let id = conf.id();
    if enregistrer_configuration_avec_id(id, conf, db) {
        return;
    }
    eprintln!("\nImpossible d'enregistrer la configuration {}", id);
}

fn enregistrer_configuration_avec_id(
    id: i64,
    conf: &Configuration,
    db: &DB
) -> bool {
    let mut s = db.prepare(
        r"INSERT OR IGNORE INTO Configuration
        (id, repartition1, repartition2, grille)
        VALUES (?, ?, ?, ?)"
    ).unwrap();
    s.bind_iter::<_, (_, sqlite::Value)>([
        (1, id.into()),
        (2, conf.repartition1.en_texte().into()),
        (3, conf.repartition2.en_texte().into()),
        (4, (conf.grille as i64).into()),
    ]).expect("Création de la requête impossible");

    s.next().is_ok()
}

