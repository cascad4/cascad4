use super::repartition::Repartition;

pub struct GenerateurConfiguration {
    pub repartitions1: Vec<Repartition>,
    pub repartitions2: Vec<Repartition>,
    pub grille: Vec<bool>,
}

impl GenerateurConfiguration {
    pub fn nouveau() -> GenerateurConfiguration {
        GenerateurConfiguration {
            repartitions1: vec![Repartition::Uniforme],
            repartitions2: vec![Repartition::Uniforme],
            grille: vec![false],
        }
    }

    pub fn generer(&self) -> Vec<Configuration> {
        let mut liste = vec![];

        for grille in &self.grille {
            for repartition1 in &self.repartitions1 {
                for repartition2 in &self.repartitions2 {
                    liste.push(Configuration {
                        repartition1: *repartition1,
                        repartition2: *repartition2,
                        grille: *grille,
                    });
                }
            }
        }

        liste
    }

    pub fn longueur(&self) -> usize {
        self.repartitions1.len()
        * self.repartitions2 .len()
        * self.grille.len()
    }
}

#[derive(Clone, Copy)]
pub struct Configuration {
    pub repartition1: Repartition,
    pub repartition2: Repartition,
    pub grille: bool,
}

impl Configuration {
    pub fn nouveau() -> Configuration {
        Configuration {
            repartition1: Repartition::Uniforme,
            repartition2: Repartition::Uniforme,
            grille: false,
        }
    }

    pub fn id(&self) -> i64 {
        self.grille as i64 + 2 * (
            self.repartition1.id() + self.repartition2.id() * 123456
        )
    }
}

