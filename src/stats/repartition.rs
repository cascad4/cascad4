use rand::{rngs::StdRng, Rng};
use rand_distr::{Distribution, Beta};

pub type Graine = <rand::rngs::StdRng as rand::SeedableRng>::Seed;

#[derive(Clone, Copy)]
pub enum Repartition {
    Uniforme,
    Beta(Beta<f32>, f32, f32),
    Segment(f32, f32),
}

impl Repartition {
    pub fn jouer(&self, rng: &mut StdRng) -> f32 {
        match self {
            Repartition::Uniforme => rng.gen_range(0.02..0.26),
            Repartition::Beta(beta, _, _) => 0.02 + beta.sample(rng) * 0.24,
            Repartition::Segment(a, b) => 0.02 + rng.gen_range(0.24*a..0.24*b),
        }
    }

    pub fn id(&self) -> i64 {
        match self {
            Repartition::Uniforme => 1,
            Repartition::Beta(_, a, b) => a.to_bits() * 123 + 4567 * b.to_bits(),
            Repartition::Segment(a, b) => a.to_bits() * 321 + 7654 * b.to_bits(),
        }.into()
    }

    pub fn en_texte(&self) -> String {
        match self {
            Repartition::Uniforme => "uniforme".into(),
            Repartition::Beta(_, a, b) => format!("beta[{},{}]", a, b),
            Repartition::Segment(a, b) => format!("segment[{},{}]", a, b),
        }
    }
}
