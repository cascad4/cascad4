use rand_distr::{Distribution, Normal};


pub struct GenerateurPerturbation {
    pub ecarts_espace: Vec<f32>,
    pub ecarts_temps: Vec<f32>,
}

impl GenerateurPerturbation {
    pub fn perturbation(&self, i: usize, j: usize) -> Perturbation {
        Perturbation {
            id: {
                self.ecarts_espace[i].to_bits() as i64
                + 123456789 * self.ecarts_temps[j].to_bits() as i64
            } % 100000,
            espace: self.ecarts_espace[i],
            temps: self.ecarts_temps[j],
            loi_espace: Normal::new(0., self.ecarts_espace[i] * 0.058).unwrap(),
            loi_temps: Normal::new(0., self.ecarts_temps[j]).unwrap(),
        }
    }

    pub fn generer(&self) -> Vec<Perturbation> {
        let mut p = vec![];
        
        for i in 0..self.ecarts_espace.len() {
            'temps: for j in 0..self.ecarts_temps.len() {
                if (i, j) == (0, 0) {
                    continue 'temps;
                }

                p.push(self.perturbation(i, j));
            }
        }

        p
    }

    pub fn longueur(&self) -> usize {
        self.ecarts_espace.len() * self.ecarts_temps.len() - 1
    }
}

pub struct Perturbation {
    pub id: i64,
    pub espace: f32,
    pub temps: f32,
    loi_espace: Normal<f32>,
    loi_temps: Normal<f32>,
}

impl Perturbation {
    pub fn spaciale(&mut self) -> f32 {
        self.loi_espace.sample(&mut rand::thread_rng()) * 0.24 + 0.02
    }
    
    pub fn temporelle(&mut self) -> isize {
        self.loi_temps.sample(&mut rand::thread_rng()).round() as isize
    }
}

