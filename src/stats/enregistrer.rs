use std::{
    ffi::OsString,
    io::{stdout, Write},
    path::{Path, PathBuf},
    sync::Arc,
    time::Instant,
};

use crate::stats::requete::{enregistrer_configuration, enregistrer_perturbation};

use super::{
    configuration::GenerateurConfiguration, partie::{
        Partie, PartiePerturbee, PartieReference
    },
    perturbation::GenerateurPerturbation,
    requete::{
        creer_tables, enregistrer_partie, enregistrer_partieperturbee, DB
    }
};

fn duree(duree: u64) -> String {
    format!(
        "{}h{:0>2}min{:0>2}sec",
        duree / 3600,
        duree / 60 % 60,
        duree % 60,
    )
}

pub struct Enregistreur {
    pub chemin_db: PathBuf,
    pub nb_fils: usize,
    pub nb_parties: usize,
    pub perturbations: GenerateurPerturbation,
    pub conf: GenerateurConfiguration,
    pub enregistrer_alignements: bool,
    pub enregistrer_pions: bool,
    db: Option<DB>
}

impl Enregistreur {
    pub fn nouveau() -> Enregistreur {
        Enregistreur {
            chemin_db: Path::new("").to_path_buf(),
            nb_fils: 4,
            nb_parties: 10,
            perturbations: GenerateurPerturbation {
                ecarts_espace: vec![0.],
                ecarts_temps: vec![0.],
            },
            conf: GenerateurConfiguration::nouveau(),
            enregistrer_alignements: false,
            enregistrer_pions: false,
            db: None,
        }
    }

    fn dossier_db(&self) -> PathBuf {
        if self.chemin_db.is_dir() {
            return self.chemin_db.clone();
        }

        self.chemin_db.clone()
            .parent()
            .unwrap_or(&Path::new(""))
            .to_path_buf()
    }

    fn generer_nom(&self) -> OsString {
        "cascad4-stat.db".into()
    }

    fn fichier_db(&self) -> OsString {
        if self.chemin_db.is_dir() {
            return self.generer_nom();
        }

        match self.chemin_db.file_name() {
            Some(nom) if nom.len() > 0 => nom.into(),
            _ => self.generer_nom(),
        }
    }

    fn creer_dossier_db(&self) {
        let dossier = self.dossier_db();

        match dossier.try_exists() {
            Ok(true) => {}
            Ok(false) => std::fs::create_dir_all(dossier)
                .expect("Impossible de créer le dossier"),
            Err(_) => panic!("Lecture du dossier impossible"),
        }
    }

    pub fn initialiser_db(&mut self) {
        self.creer_dossier_db();
        let mut chemin = self.dossier_db();
        chemin.push(self.fichier_db());

        match sqlite::open(chemin) {
            Ok(db) => self.db = Some(db),
            Err(err) => panic!(
                "Impossible d'ouvrir la base de données: {}",
                err,
            ),
        }
    }

    pub fn enregistrer(&mut self) {
        let db = match &self.db {
            Some(db) => db,
            None => panic!("Base de donnée pas initialisée"),
        };

        creer_tables(&db);

        let mut progres = 0.;
        let debut = Instant::now();

        let mut attente = Vec::with_capacity(self.nb_parties);
        let mut active = Vec::with_capacity(self.nb_fils);

        println!("Ajout des parties de référence à la pile d'attente...");
        for conf in self.conf.generer() {
            for _ in 0..self.nb_parties {
                attente.push(
                    PartieReference::nouveau(conf)
                        .en_partie()
                        .lanceur()
                );
            }
        }

        println!(
            "Enregisrement de {} perturbations...",
            self.perturbations.longueur(),
        );
        for p in self.perturbations.generer() {
            enregistrer_perturbation(
                p.id,
                p.espace,
                p.temps,
                &db
            );
        }
        println!(
            "Enregistrement de {} configurations...",
            self.conf.longueur(),
        );
        for c in self.conf.generer() {
            enregistrer_configuration(&c, &db);
        }

        let mut stdout = stdout().lock();
        let nb_total = {
            (self.perturbations.longueur() + 1)
            * self.conf.longueur()
            * self.nb_parties
        } as f32;

        println!("Enregistrement des parties:");
        'principale: while attente.len() + active.len() > 0 {
            // On lance des parties tant que des emplacements sont disponibles
            'lancement: while active.len() < self.nb_fils {
                match attente.pop() {
                    Some(lanceur) => active.push(
                        std::thread::spawn(lanceur)
                    ),
                    None => break 'lancement,
                }
            }

            // On cherche une partie finie et on l'enregistre
            for i in 0..active.len() {
                if active[i].is_finished() {
                    let partie = active
                        .remove(i)
                        .join()
                        .expect("Fil irrécupérable");

                    match partie {
                        Partie::Reference(partie) => {
                            enregistrer_partie(
                                &partie,
                                self.enregistrer_alignements,
                                self.enregistrer_pions,
                                &db
                            );

                            let partie = Arc::new(partie);

                            // Ajout des parties perturbées à la pile d'attente
                            for perturbation in self.perturbations.generer() {
                                attente.push(
                                    PartiePerturbee::nouveau(
                                        partie.clone(),
                                        perturbation
                                    ).en_partie().lanceur()
                                );
                            }
                        },
                        Partie::Perturbee(partie) => {
                            enregistrer_partieperturbee(&partie, &db);
                        },
                    }

                    progres += 1.;
                    write!(
                        stdout,
                        "\rProgression: {:.2}/{}, ETA {: >15}, {:0.2} parties/s",
                        progres / (
                            (self.perturbations.longueur() + 1)
                            * self.conf.longueur()
                        ) as f32,
                        self.nb_parties,
                        duree({
                            (nb_total / progres - 1.)
                            * debut.elapsed().as_secs_f32()
                        } as u64),
                        progres / debut.elapsed().as_secs_f32(),
                    ).unwrap();
                    stdout.flush().unwrap();
                    continue 'principale;
                }
            }

            std::thread::sleep(std::time::Duration::from_millis(50));
        }

        println!(
            "\n{}×{}×{} parties enregistrées en {} ({:.2} ms/partie).",
            self.nb_parties,
            self.perturbations.longueur() + 1,
            self.conf.longueur(),
            duree(debut.elapsed().as_secs()),
            debut.elapsed().as_secs_f32() * 1000.
            / (self.nb_parties * (self.perturbations.longueur() + 1)
            * self.conf.longueur()) as f32,
        );
    }
}

