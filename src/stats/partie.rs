use std::sync::Arc;

use rand::Rng;

use crate::jeu::{
    alignement::alignements_quelconques,
    commun::objet::Pion,
    fin::Resultat,
    jeu::{Jeu, MoteurJeu}
};

use super::{
    configuration::Configuration,
    perturbation::Perturbation,
    repartition::Graine
};


pub enum Partie {
    Reference(PartieReference),
    Perturbee(PartiePerturbee),
}

impl Partie {
    pub fn lanceur(mut self) -> impl FnOnce() -> Partie {
        move || {
            self.jouer();
            self
        }
    }

    pub fn jouer(&mut self) {
        match self {
            Partie::Reference(partie) => partie.jouer(),
            Partie::Perturbee(partie) => partie.jouer(),
        }
    }
}

fn arrondir(x: f32) -> f32 {
    ((x - 0.02) / 0.04).round() * 0.04 + 0.02
}

pub struct PartieReference {
    jeu: Jeu,
    conf: Configuration,
    graine: Graine,
}

impl PartieReference {
    pub fn nouveau(conf: Configuration) -> PartieReference {
        PartieReference {
            jeu: Jeu::nouveau(),
            conf,
            graine: rand::thread_rng().gen(),
        }
    }

    pub fn en_partie(self) -> Partie {
        Partie::Reference(self)
    }

    pub fn jouer(&mut self) {
        let mut moteur = MoteurJeu::nouveau_avec_jeu(&self.jeu);

        if self.conf.grille {
            moteur.initialiser_grille();
        } else {
            moteur.initialiser();
        }

        let mut rng_partie = rand::SeedableRng::from_seed(self.graine);

        while !moteur.partie_finie() {
            let x = if moteur.jeu().tour % 2 == 0 {
                self.conf.repartition1.jouer(&mut rng_partie)
            } else {
                self.conf.repartition2.jouer(&mut rng_partie)
            };
            
            if self.conf.grille {
                moteur.deplacer_pion_suivant(arrondir(x));
            } else {
                moteur.deplacer_pion_suivant(x);
            }

            moteur.jouer_pion_suivant(false);
            for _ in 0..100 {
                moteur.actualiser();
            }
        }

        self.jeu = moteur.jeu().clone();
    }

    pub fn longueur(&self) -> usize {
        self.jeu.pions.len()
    }

    pub fn resultat(&self) -> Resultat {
        self.jeu.resultat()
    }

    pub fn id(&self) -> i64 {
        let mut id = 0;
        for i in 0..31 {
            id += {
                self.graine[i] as i64
                * 2i64.pow(8 * (i % 4) as u32)
                * i as i64
            };
        }
        id
    }

    pub fn conf_id(&self) -> i64 {
        self.conf.id()
    }

    pub fn pions(&self) -> &Vec<Pion> {
        &self.jeu.pions
    }

    pub fn alignements(&self) -> Vec<Vec<usize>> {
        alignements_quelconques(self.pions())
    }
}

pub struct PartiePerturbee {
    jeu: Jeu,
    pub perturbation: Perturbation,
    conf: Configuration,
    graine: Graine,
    reference: Arc<PartieReference>,
}

impl PartiePerturbee {
    pub fn nouveau(
        principale: Arc<PartieReference>,
        perturbation: Perturbation
    ) -> PartiePerturbee {
        PartiePerturbee {
            jeu: Jeu::nouveau(),
            perturbation,
            conf: principale.conf,
            graine: principale.graine,
            reference: principale,
        }
    }

    pub fn en_partie(self) -> Partie {
        Partie::Perturbee(self)
    }

    pub fn jouer(&mut self) {
        let mut moteur = MoteurJeu::nouveau_avec_jeu(&self.jeu);

        if self.conf.grille {
            moteur.initialiser_grille();
        } else {
            moteur.initialiser();
        }

        let mut rng_partie = rand::SeedableRng::from_seed(self.graine);

        while !moteur.partie_finie() {
            let x = if moteur.jeu().tour % 2 == 0 {
                self.conf.repartition1.jouer(&mut rng_partie)
            } else {
                self.conf.repartition2.jouer(&mut rng_partie)
            };
            
            if self.conf.grille {
                moteur.deplacer_pion_suivant(
                    arrondir(x + self.perturbation.spaciale())
                );
            } else {
                moteur.deplacer_pion_suivant(
                    x + self.perturbation.spaciale()
                )
            }

            moteur.jouer_pion_suivant(false);
            for _ in 0..(100 + self.perturbation.temporelle()) {
                moteur.actualiser();
            }
        }

        self.jeu = moteur.jeu().clone();
    }

    pub fn longueur(&self) -> usize {
        self.jeu.pions.len()
    }

    pub fn resultat(&self) -> Resultat {
        self.jeu.resultat()
    }

    pub fn id(&self) -> i64 {
        self.reference.id()
    }

    pub fn conf_id(&self) -> i64 {
        self.conf.id()
    }

    pub fn score(&self) -> f64 {
        let terme1;
        if self.resultat() == self.reference.resultat() {
            terme1 = 1.0;
        } else {
            terme1 = 2.0;
        }

        let terme2 = 1.0 + 0.05 * {
            self.longueur().abs_diff(self.reference.longueur())
        } as f64;

        terme1 * terme2 - 1.0
    }
}

