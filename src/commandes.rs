use std::env::Args;
use std::path::PathBuf;
use rand_distr::Beta;

use crate::stats::repartition::Repartition;


type Resultat<U>=Result<Option<U>, String>;

pub enum ArgumentStat {
    CheminDB(PathBuf),
    EcartsEspace(ListeFlottantsPositif),
    EcartsTemps(ListeFlottantsPositif),
    NbFils(EntierStrictementPositif),
    NbParties(EntierPositif),
    Repartitions1(Repartitions),
    Repartitions2(Repartitions),
    Grille(ListeBool),
}

impl ArgumentStat {
    pub fn extraire(args: &mut Args) -> Resultat<ArgumentStat> {
        let arg = args.next();

        match arg {
            Some(base) if base.starts_with("--") => {
                ArgumentStat::extraire_argument_cle(&base)
            },
            Some(base) if base.starts_with("-") => {
                ArgumentStat::extraire_argument(&base, args)
            },
            Some(base) => ArgumentStat::extraire_argument_defaut(&base),
            None => Ok(None),
        }
    }

    fn extraire_argument_cle(arg: &str) -> Resultat<ArgumentStat> {
        let valeurs: Vec<&str> = arg.split("=").collect();
        let cle = valeurs.get(0);
        let valeur = valeurs.get(1);

        fn info_liste(arg: &str) -> Resultat<ArgumentStat> {
            Err(format!("Utilisation: {}=<1>,<2>,...,<N>", arg))
        }

        fn info(arg: &str) -> Resultat<ArgumentStat> {
            Err(format!("Utilisation: {}=<valeur>", arg))
        }

        match (cle, valeur) {
            (Some(&"--grille"), Some(valeur)) => {
                match ListeBool::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::Grille(liste))),
                    Err(msg) => Err(format!("Erreur dans --grille: {}", msg)),
                }
            },
            (Some(&"--grille"), None) => info_liste("--grille"),
            (Some(&"--espace"), Some(valeur)) => {
                match ListeFlottantsPositif::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::EcartsEspace(liste))),
                    Err(msg) => Err(format!("Erreur dans --espace: {}", msg)),
                }
            },
            (Some(&"--espace"), None) => info_liste("--espace"),
            (Some(&"--temps"), Some(valeur)) => {
                match ListeFlottantsPositif::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::EcartsTemps(liste))),
                    Err(msg) => Err(format!("Erreur dans --temps: {}", msg)),
                }
            },
            (Some(&"--temps"), None) => info_liste("--temps"),
            (Some(&"--nb-fils"), Some(valeur)) => {
                match EntierStrictementPositif::nouveau(valeur) {
                    Ok(entier) => Ok(Some(ArgumentStat::NbFils(entier))),
                    Err(msg) => Err(format!("Erreur dans --nb-fils: {}", msg)),
                }
            },
            (Some(&"--nb-fils"), None) => info("--nb-fils"),
            (Some(&"--nb-parties"), Some(valeur)) => {
                match EntierPositif::nouveau(valeur) {
                    Ok(entier) => Ok(Some(ArgumentStat::NbParties(entier))),
                    Err(msg) => Err(format!("Erreur dans --nb-parties: {}", msg)),
                }
            },
            (Some(&"--nb-parties"), None) => info("--nb-parties"),
            (Some(&"--chemin-db"), Some(valeur)) => {
                Ok(Some(ArgumentStat::CheminDB(PathBuf::from(valeur))))
            },
            (Some(&"--chemin-db"), None) => info("--chemin-db"),
            (Some(&"--rep1"), Some(valeur)) => {
                match Repartitions::nouveau(valeur) {
                    Ok(rep) => Ok(Some(ArgumentStat::Repartitions1(rep))),
                    Err(msg) => Err(format!("Erreur dans --rep1: {}", msg)),
                }
            },
            (Some(&"--rep1"), None) => info_liste("--rep1"),
            (Some(&"--rep2"), Some(valeur)) => {
                match Repartitions::nouveau(valeur) {
                    Ok(rep) => Ok(Some(ArgumentStat::Repartitions2(rep))),
                    Err(msg) => Err(format!("Erreur dans --rep2: {}", msg)),
                }
            },
            (Some(&"--rep2"), None) => info_liste("--rep2"),
            (Some(&cle), _) => Err(format!("Argument inconnu: {}", cle)),
            (None, _) => Ok(None),
        }
    }

    fn extraire_argument(arg: &str, args: &mut Args) -> Resultat<ArgumentStat> {
        fn info_liste(arg: &str) -> Resultat<ArgumentStat> {
            Err(format!("Utilisation: {} <1>,<2>,...,<N>", arg))
        }

        fn info(arg: &str) -> Resultat<ArgumentStat> {
            Err(format!("Utilisation: {} <valeur>", arg))
        }

        match (arg, args.nth(0).as_deref()) {
            ("-g",  Some(valeur)) => {
                args.next();
                match ListeBool::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::Grille(liste))),
                    Err(msg) => Err(format!("Erreur dans -g: {}", msg)),
                }
            },
            ("-g", None) => info_liste("-g"),
            ("-e", Some(valeur)) => {
                args.next();
                match ListeFlottantsPositif::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::EcartsEspace(liste))),
                    Err(msg) => Err(format!("Erreur dans -e: {}", msg)),
                }
            },
            ("-e", None) => info_liste("-e"),
            ("-t", Some(valeur)) => {
                args.next();
                match ListeFlottantsPositif::nouveau(valeur) {
                    Ok(liste) => Ok(Some(ArgumentStat::EcartsTemps(liste))),
                    Err(msg) => Err(format!("Erreur dans -t: {}", msg)),
                }
            },
            ("-t", None) => info_liste("-t"),
            ("-f", Some(valeur)) => {
                args.next();
                match EntierStrictementPositif::nouveau(valeur) {
                    Ok(entier) => Ok(Some(ArgumentStat::NbFils(entier))),
                    Err(msg) => Err(format!("Erreur dans -f: {}", msg)),
                }
            },
            ("-f", None) => info("-f"),
            ("-n", Some(valeur)) => {
                args.next();
                match EntierPositif::nouveau(valeur) {
                    Ok(entier) => Ok(Some(ArgumentStat::NbParties(entier))),
                    Err(msg) => Err(format!("Erreur dans -n: {}", msg)),
                }
            },
            ("-n", None) => info("-n"),
            ("-c", Some(valeur)) => {
                Ok(Some(ArgumentStat::CheminDB(PathBuf::from(valeur))))
            },
            ("-c", None) => info("-c"),
            ("-1", Some(valeur)) => {
                args.next();
                match Repartitions::nouveau(valeur) {
                    Ok(rep) => Ok(Some(ArgumentStat::Repartitions1(rep))),
                    Err(msg) => Err(format!("Erreur dans -1: {}", msg)),
                }
            },
            ("-1", None) => info_liste("-1"),
            ("-2", Some(valeur)) => {
                args.next();
                match Repartitions::nouveau(valeur) {
                    Ok(rep) => Ok(Some(ArgumentStat::Repartitions2(rep))),
                    Err(msg) => Err(format!("Erreur dans -2: {}", msg)),
                }
            },
            ("-2", None) => info_liste("-2"),
            (cle, _) => Err(format!("Argument inconnu: {}", cle)),
        }
    }

    fn extraire_argument_defaut(arg: &str) -> Resultat<ArgumentStat> {
        match EntierPositif::nouveau(arg) {
            Ok(entier) => Ok(Some(ArgumentStat::NbParties(entier))),
            Err(msg) => Err(format!("Erreur dans [nbparties]: {}", msg)),
        }
    }
}

pub struct ListeFlottantsPositif {
    liste: Vec<f32>,
}

impl ListeFlottantsPositif {
    pub fn nouveau(arg: &str) -> Result<ListeFlottantsPositif, String> {
        let valeurs: Vec<&str> = arg.split(",").collect();
        let mut liste = vec![];
        
        for v in valeurs {
            match v.parse::<f32>() {
                Ok(nb) if nb >= 0. => {
                    if liste.contains(&nb) {
                        eprintln!("1 valeur redondante ignorée");
                    } else {
                        liste.push(nb);
                    }
                },
                Ok(nb) => return Err(format!("{} est négatif", nb)),
                Err(_) => return Err(format!("'{}' n'est pas un flottant", v)),
            }
        }

        Ok(ListeFlottantsPositif { liste })
    }

    pub fn en_liste(self) -> Vec<f32> {
        self.liste
    }
}

pub struct ListeBool {
    liste: Vec<bool>,
}

impl ListeBool {
    pub fn nouveau(arg: &str) -> Result<ListeBool, String> {
        let valeurs: Vec<&str> = arg.split(",").collect();
        let mut liste = vec![];
        
        for v in valeurs {
            match v.parse::<bool>() {
                Ok(b) => {
                    if liste.contains(&b) {
                        eprintln!("1 valeur redondante ignorée");
                    } else {
                        liste.push(b);
                    }
                },
                Err(_) => return Err(format!("'{}' n'est pas un booléen", v)),
            }
        }

        Ok(ListeBool { liste })
    }

    pub fn en_liste(self) -> Vec<bool> {
        self.liste
    }
}

pub struct EntierPositif {
    entier: usize,
}

impl EntierPositif {
    pub fn nouveau(arg: &str) -> Result<EntierPositif, String> {
        let entier = match arg.parse::<isize>() {
            Ok(nb) if nb >= 0 => nb,
            Ok(nb) => return Err(format!("{} est négatif", nb)),
            Err(_) => return Err(format!("'{}' n'est pas un entier", arg)),
        } as usize;
        
        Ok(EntierPositif { entier })
    }

    pub fn en_entier(&self) -> usize {
        self.entier
    }
}

pub struct EntierStrictementPositif {
    entier: usize,
}

impl EntierStrictementPositif {
    pub fn nouveau(arg: &str) -> Result<EntierStrictementPositif, String> {
        let entier = match arg.parse::<isize>() {
            Ok(nb) if nb > 0 => nb,
            Ok(0) => return Err(format!("l'entier est nul")),
            Ok(nb) => return Err(format!("{} est négatif", nb)),
            Err(_) => return Err(format!("'{}' n'est pas un entier", arg)),
        } as usize;
        
        Ok(EntierStrictementPositif { entier })
    }

    pub fn en_entier(&self) -> usize {
        self.entier
    }
}

pub struct Repartitions {
    liste: Vec<Repartition>,
}

impl Repartitions {
    pub fn nouveau(arg: &str) -> Result<Repartitions, String> {
        let mut liste = vec![];

        let mut i = 0;
        let mut j = 0;
        while j < arg.len() {
            let parametre;
            'repartition: loop {
                match arg.get(j..=j) {
                    Some(",") | None => {
                        parametre = false;
                        break 'repartition;
                    },
                    Some("[") => {
                        parametre = true;
                        break 'repartition;
                    },
                    _ => (),
                }
                j += 1;
            }
            let repartition = arg.get(i..j).unwrap_or_default();
            j += 1;
            i = j;
            let mut parametres = vec![];
            if parametre {
                'parametre: loop {
                    match arg.get(j..=j) {
                        Some("]") => break 'parametre,
                        None => {
                            return Err(format!(
                                "Crochet non refermée: {}",
                                arg.get(i..arg.len()).unwrap_or_default()
                            ));
                        },
                        _ => (),
                    }
                    j += 1;
                }
                
                let valeurs: Vec<&str> = arg.get(i..j)
                    .unwrap_or_default()
                    .split(",")
                    .collect();
                j += 1;
                i = j;

                for v in valeurs {
                    match v.parse::<f32>() {
                        Ok(nb) => {
                            parametres.push(nb);
                        },
                        Err(_) => {
                            return Err(format!("'{}' n'est pas un flottant", v));
                        },
                    }
                }
            }

            match repartition {
                "uniforme" => {
                    if parametres.len() != 0 {
                        return Err(format!(
                            "uniforme prend 0 arguments ({} passés)",
                            parametres.len(),
                        ));
                    }

                    liste.push(Repartition::Uniforme);
                },
                "segment" => {
                    if parametres.len() != 2 {
                        return Err(format!(
                            "segment prend 2 arguments ({} passés)",
                            parametres.len(),
                        ));
                    }
                    let (a, b) = (parametres[0], parametres[1]);
                    if a < 0. || a > 1. || b < 0. || b > 1. {
                        return Err(format!(
                            "Les arguments de segment doivent être entre 0 et 1"
                        ));
                    }
                    
                    liste.push(
                        Repartition::Segment(f32::min(a, b), f32::max(a, b))
                    );
                },
                "beta" => {
                    if parametres.len() != 2 {
                        return Err(format!(
                            "beta prend 2 arguments ({} passés)",
                            parametres.len(),
                        ));
                    }
                    let (a, b) = (parametres[0], parametres[1]);

                    liste.push(match beta(a, b) {
                        Ok(beta) => beta,
                        Err(msg) => return Err(msg),
                    });
                },
                "betas" => {
                    if parametres.len() != 8 {
                        return Err(format!(
                            "betas prend 8 arguments ({} passés)",
                            parametres.len(),
                        ));
                    }
                    let a_min = parametres[0];
                    let a_max = parametres[1];
                    let a_r = parametres[2];
                    let a_q = parametres[3];
                    let b_min = parametres[4];
                    let b_max = parametres[5];
                    let b_r = parametres[6];
                    let b_q = parametres[7];

                    let (mut a, mut b);
                    
                    b = b_min;
                    while b < b_max {
                        b = b * b_q + b_r;
                        a = a_min;
                        while a < a_max {
                            a = a * a_q + a_r;
                            liste.push(match beta(a, b) {
                                Ok(beta) => beta,
                                Err(msg) => return Err(msg),
                            });
                        }
                    }
                }
                "" => {
                    continue;
                },
                autre => {
                    return Err(format!("Répartiton inconnue: {}", autre));
                },
            }
        }

        Ok(Repartitions { liste })
    }

    pub fn en_liste(self) -> Vec<Repartition> {
        self.liste
    }
}

fn beta(a: f32, b: f32) -> Result<Repartition, String> {
    Ok(Repartition::Beta(
        match Beta::new(a, b) {
            Ok(beta) => beta,
            Err(msg) => {
                return Err(format!(
                    "paramètres incorrects pour beta: {}",
                    msg,
                ));
            }
        },
        a,
        b,
    ))
}
